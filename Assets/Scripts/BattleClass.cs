﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleClass : MonoBehaviour
{
	public class BattleData
	{
		public PlayerClass.PCData Player
		{
			get
			{
				return PlayerClass.PCDataInstance;
			}
		}

		int CurrentTurn; // 해당 전투의 몇 번째 차례인가
		int UserChoice;
		int ItemChoice;
		bool UserInput;
		public MonsterClass TargetMonster;
		List<MonsterClass> CurrentDungeonMonsters {get { return MonsterClass.MonsterInstanceList;}}
		List<ItemClass> DroppedItemList;

		public BattleData()
		{
			UserInput = false;
			CurrentTurn = 1;
		}

		public bool DungeonLoop()
		{
			if (!Round ())
			{
				Debug.Log ("게임 망함ㅎ");
				return false;
			}

			return true;
		}

		public bool Round()
		{
			if (UserInput)
			{
				PlayerTurn ();

				if (Player.IsDead ())
					return false;
				
				MonsterRemove ();
				MonsterAttack ();

				if (Player.IsDead ())
					return false;

				CurrentTurn++;
				UserInput = false;
			}

			return true;
		}

		public void PlayerTurn()
		{
			switch (UserChoice)
			{
			case 0: // Basic Attack
				BasicAttack();
				break;
			case 1: // Bless
				DoWhatGodWant();
				break;
			case 2: // Use Item
				UseItem();
				break;
			}
		}
		private void MonsterRemove()
		{
			MonsterClass.MonsterInstanceList.RemoveAll (obj => obj.IsDead);
		}

		public bool IsClear()
		{
			if (CurrentDungeonMonsters.Count == 0)
				return true;
			return false;
		}

		public void Action(int action)
		{
			UserChoice = action;
			UserInput = true;
		}
		public void Action(int action, MonsterClass m)
		{
			TargetMonster = m;
			UserChoice = action;
			UserInput = true;
		}

		public void Action(int action, int i)
		{
			ItemChoice = i;	
			UserChoice = action;
			UserInput = true;
		}

		private void BasicAttack()
		{
			Debug.Log ("call");
			if (TargetMonster == null)
			{
				UserInput = false;
				return;
			}
			Debug.Log ("Target");
			if (Player.Weapon == null)
			{
				TargetMonster.GetDamage (Player.Power, DamageType.N);
				return;
			}

			int prob = (int)Random.Range(0, 100);
			switch(Player.Weapon.WeaponT)
			{
			case WeaponType.Spear:
				if (prob <= 25)
					TargetMonster.GetDamage (Player.Power, Player.PowerType, 0);
				else
					TargetMonster.GetDamage (Player.Power, Player.PowerType);
				break;
			case WeaponType.Ax:
				if (prob <= 25)
				{
					foreach (MonsterClass m in CurrentDungeonMonsters)
						m.GetDamage (Player.Power, Player.PowerType);
				}
				else
					TargetMonster.GetDamage (Player.Power, Player.PowerType);
				break;
			case WeaponType.Dagger:
				if (prob <= 25)
					TargetMonster.GetDamage (2 * Player.Power, Player.PowerType);
				else
					TargetMonster.GetDamage (Player.Power, Player.PowerType);
				break;
			case WeaponType.LongSword:
			case WeaponType.Hammer:
				TargetMonster.GetDamage (Player.Power, Player.PowerType);
				break;
			}


			CurrentDungeonMonsters.RemoveAll (obj => obj.IsDead);

			Player.WeaponTear ();
		}
		private void DoWhatGodWant() 
		{
			Player.Pray ();
			return;
		}

		public void UseItem()
		{
			ConsumableClass I = (ConsumableClass)PlayerClass.PCDataInstance.Inventory [ItemChoice];
			I.ItemEffect();
            Player.Inventory.RemoveAt(ItemChoice);
            return;
		}

		private void MonsterAttack()
		{
			foreach (MonsterClass Monster in CurrentDungeonMonsters)
			{
				
				int prob = (int)Random.Range (0, 100);

				switch(Monster.Skill())
				{
				case SkillType.Penetrate:
					if (prob <= 15)
						Player.GetDamage (Monster.Power, Monster.PowerType, 0);
					else
						Player.GetDamage (Monster.Power, Monster.PowerType);
					break;
				case SkillType.RemoveSpirit:
					if (prob <= 15)
					{
						DamageType KillGuard;
						while(true)
						{
							KillGuard = (DamageType)(1 << (int)Random.Range (0, 4));
							if ((KillGuard & Player.ImmuneType) != 0)
								break;
						}
						Player.ImmuneType = (DamageType)(Player.ImmuneType - KillGuard);
					}
					Player.GetDamage (Monster.Power, Monster.PowerType);
					break;
				case SkillType.SelfHeal:
					if (prob <= 25)
						Monster.hp += 5;
					if (Monster.hp > Monster.maxhp)
						Monster.hp = Monster.maxhp;
					Player.GetDamage (Monster.Power, Monster.PowerType);
					break;
				case SkillType.Revenge:
					int cHP = Player.hp;
					Player.GetDamage (Monster.Power, Monster.PowerType);
					if (Player.hp == cHP)
					{
						DamageType KillGuard;
						while(true)
						{
							KillGuard = (DamageType)(1 << (int)Random.Range (0, 4));
							if ((KillGuard & Player.ImmuneType) != 0)
								break;
						}
						Player.ImmuneType = (DamageType)(Player.ImmuneType - KillGuard);
						Monster.ImmuneType = (DamageType)(Monster.ImmuneType | KillGuard);
					}
					break;
				case SkillType.UndeadHeal:
				case SkillType.BlessOfSpirit:
					Player.GetDamage (Monster.Power, Monster.PowerType);
					break;
				}

				Player.ArmorTear ();
			}
		}
	}
}
