﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ButtonClickScript : MonoBehaviour {

    private BattleManager bm;
    private string thisName;
	// Use this for initialization
	void Start ()
    {
        bm = GameObject.Find("GameController").GetComponent<BattleManager>();
        thisName = gameObject.name;
	}
	

    void OnMouseUpAsButton()
    {
        bm.ManageButtonClick(thisName);
    }
    void OnMouseOver()
    {
        bm.ManageMouseOver(thisName);
    }
    void OnMouseExit()
    {
        bm.ManageMouseExit();
    }
}
