﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public delegate void Del();
public class BlessClass
{
	
    BlessClass(int weight) : this(1, 5, weight) { }

    BlessClass(int minL, int maxL, int weight)
    {
        MinLevel = minL;
        MaxLevel = maxL;
        Weight = weight;
    }

    BlessClass(int lvl, BlessClass b) : this(b.MinLevel, b.MaxLevel, b.Weight)
    {
        Level = lvl;
    }

	BlessClass (DamageType d, BlessClass b) : this(b.MinLevel, b.MaxLevel, b.Weight)
	{
		BlessDamageType = d;
	}

    public Del Effect;
    public int MinLevel;
    public int MaxLevel;
    public int Level;
    public int Weight;
	public int coeff;
	public DamageType BlessDamageType;
	public string Description;
	public bool LevelDependency;
	public bool TypeDependency;

    public bool IsValidLevel(int l)
    {
        return (MinLevel <= l) && (l <= MaxLevel);
    }

	public string GetInfo()
	{
		int val = coeff * Level;
		string ret;
		string Fval = val.ToString ();
		if (LevelDependency) {
			ret = Description + Fval;
			if (TypeDependency)
				ret += " 피해.";
			return ret;
		} else {
			if (TypeDependency) {
				string type = "";
				switch (BlessDamageType) {
				case DamageType.T1:
					type = "Green";
					break;
				case DamageType.T2:
					type = "Red";
					break;
				case DamageType.T3:
					type = "Blue";
					break;
				case DamageType.T4:
					type = "Brown";
					break;
				}
				ret = Description + type + " 속성으로 " + Fval + " 피해.";
				return ret;
			} else
				return Description;
		}
	}

    static List<BlessClass> _allBlessPool = new List<BlessClass>();
    public static void PoolInitialize()
    {
		// Heal Random unit 10 * lev
		var b = new BlessClass(1, 5, 40);
		b.LevelDependency = true;
		b.Level = 1;
		b.coeff = 20;
		b.Description = "임의의 유닛 체력 회복 ";
		b.Effect += () => 
		{ 
			
			int maxindex = MonsterClass.MonsterInstanceList.Count - 1;
			int index = (int)Random.Range (0, maxindex);
			if (index == maxindex)
			{
				PlayerClass.PCDataInstance.hp += b.Level * 20;
                if(PlayerClass.PCDataInstance.hp > PlayerClass.PCDataInstance.maxhp)
                    PlayerClass.PCDataInstance.hp = PlayerClass.PCDataInstance.maxhp;

            }
			else
			{
				MonsterClass.MonsterInstanceList [index].hp += b.Level * 10;
                if (MonsterClass.MonsterInstanceList[index].hp > MonsterClass.MonsterInstanceList[index].maxhp)
                    MonsterClass.MonsterInstanceList[index].hp = MonsterClass.MonsterInstanceList[index].maxhp;
            } 
		};
		_allBlessPool.Add (b);

		// Heal Everyunit 5 * lev
		b = new BlessClass(1, 5, 40);
		b.LevelDependency = true;
		b.Level = 1;
		b.coeff = 10;
		b.Description = "모든 유닛의 체력회복 ";
		b.Effect += () =>
		{
			PlayerClass.PCDataInstance.hp += b.Level * 10;
            if (PlayerClass.PCDataInstance.hp > PlayerClass.PCDataInstance.maxhp)
                PlayerClass.PCDataInstance.hp = PlayerClass.PCDataInstance.maxhp;
            foreach (MonsterClass m in MonsterClass.MonsterInstanceList)
			{
				m.hp += b.Level * 10;
                if (m.hp > m.maxhp)
                    m.hp = m.maxhp;
            }
		};
		_allBlessPool.Add (b);

		// Heal Random Enemy unit 5 * lev
		b = new BlessClass(1, 5, 40);
		b.LevelDependency = true;
		b.Level = 1;
		b.coeff = 20;
		b.Description = "임의의 적 유닛 체력 회복 ";
		b.Effect += () =>
		{ 
			
			int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
			MonsterClass.MonsterInstanceList [index].hp += b.Level * 20;
            if (MonsterClass.MonsterInstanceList[index].hp > MonsterClass.MonsterInstanceList[index].maxhp)
                MonsterClass.MonsterInstanceList[index].hp = MonsterClass.MonsterInstanceList[index].maxhp;

        };
		_allBlessPool.Add (b);

		// Heal Player 10 * lev
		b = new BlessClass(3, 5, 20);
		b.LevelDependency = true;
		b.Level = 1;
		b.coeff = 20;
		b.Description = "플레이어의 체력회복 ";
		b.Effect += () =>
		{
			PlayerClass.PCDataInstance.hp += b.Level * 20;
            if (PlayerClass.PCDataInstance.hp > PlayerClass.PCDataInstance.maxhp)
                PlayerClass.PCDataInstance.hp = PlayerClass.PCDataInstance.maxhp;

        };
		_allBlessPool.Add(b);


		// Damage Random unit 5 * lev with random Type
		b = new BlessClass(1, 5, 30);
		b.LevelDependency = true;
		b.TypeDependency = true;
		b.Level = 1;
		b.coeff = 20;
		b.Description = "임의의 유닛에게 ";
		b.Effect += () => 
		{
			
			int maxindex = MonsterClass.MonsterInstanceList.Count - 1;
			int index = (int)Random.Range (0, maxindex);
			if (index == maxindex)
			{
				PlayerClass.PCDataInstance.GetDamage( b.Level * 20, b.BlessDamageType);
			}
			else
			{
				MonsterClass.MonsterInstanceList [index].GetDamage(b.Level * 20, b.BlessDamageType);
			}
		};
		_allBlessPool.Add (b);

		// Deal Random Enemy unit 5 * lev
		b = new BlessClass(2, 5, 10);
		b.LevelDependency = true;
		b.Level = 1;
		b.TypeDependency = true;
		b.coeff = 20;
		b.Description = "임의의 적 유닛에게 ";
		b.Effect += () =>
		{
			
			int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
			MonsterClass.MonsterInstanceList [index].GetDamage(b.Level * 20, b.BlessDamageType);
		};
		_allBlessPool.Add (b);

		// Deal Everyunit 5 * lev with type
		b = new BlessClass(1, 5, 30);
		b.LevelDependency = true;
		b.TypeDependency = true;
		b.Level = 1;
		b.coeff = 10;
		b.Description = "모든 유닛에게 ";
		b.Effect += () =>
		{
			PlayerClass.PCDataInstance.GetDamage(b.Level * 10, b.BlessDamageType);
			foreach(MonsterClass m in MonsterClass.MonsterInstanceList)
			{
				m.GetDamage( b.Level * 10, b.BlessDamageType);
			}
		};
		_allBlessPool.Add (b);

		// Deal Player 5 * lev
		b = new BlessClass(1, 5, 15);
		b.LevelDependency = true;
		b.TypeDependency = true;
		b.coeff = 20;
		b.Level = 1;
		b.Description = "플레이어에게 ";
		b.Effect += () =>
		{
			PlayerClass.PCDataInstance.GetDamage( b.Level * 20, b.BlessDamageType);
		};
		_allBlessPool.Add(b);

		//Add Buff Random unit with random Type
		b = new BlessClass(1, 5, 20);
		b.LevelDependency = false;
		b.Level = 1;
		b.Description = "임의의 유닛에게 속성 저항 부여.";
		b.Effect += () => 
		{
			
			int maxindex = MonsterClass.MonsterInstanceList.Count - 1;
			int index = (int)Random.Range (0, maxindex);
			DamageType newImmune = (DamageType)(1 << (int)Random.Range(0,4));
			if (index == maxindex)
			{
				PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType | newImmune);
			}
			else
			{
				MonsterClass.MonsterInstanceList [index].ImmuneType = (DamageType)(MonsterClass.MonsterInstanceList [index].ImmuneType | newImmune) ;
			}
		};
		_allBlessPool.Add (b);


		// Add Buff Everyunit with type
		b = new BlessClass(3, 5, 15);
		b.LevelDependency = false;
		b.Level = 1;
		b.Description = "모든 유닛에게 속성 저항 하나 부여.";
		b.Effect += () =>
		{
			
			DamageType newImmune = (DamageType)(1 << (int)Random.Range(0,4));
			PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType | newImmune);
			foreach(MonsterClass m in MonsterClass.MonsterInstanceList)
			{
				m.ImmuneType = (DamageType)(m.ImmuneType | newImmune) ;
			}
		};
		_allBlessPool.Add (b);

		// Add buff Player 
		b = new BlessClass(4, 5, 5);
		b.Level = 1;
		b.LevelDependency = false;
		b.Description = "플레이어에게 속성 저항 하나 부여.";
		b.Effect += () =>
		{
			
			DamageType newImmune = (DamageType)(1 << (int)Random.Range(0,4));
			PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType | newImmune);
		};
		_allBlessPool.Add(b);

		////////

		// Remove one Buff Random unit with random Type
		b = new BlessClass(1, 3, 20);
		b.LevelDependency = false;
		b.Level = 1;
		b.Description = "임의의 유닛 저항 하나 제거";
		b.Effect += () => 
		{
			
			int maxindex = MonsterClass.MonsterInstanceList.Count - 1;
			int index = (int)Random.Range (0, maxindex);
			DamageType newImmune = (DamageType)(1 << (int)Random.Range(0,4));
			if (index == maxindex)
			{
				PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType & ~ newImmune);
			}
			else
			{
				MonsterClass.MonsterInstanceList [index].ImmuneType = (DamageType)(MonsterClass.MonsterInstanceList [index].ImmuneType & ~newImmune) ;
			}
		};
		_allBlessPool.Add (b);

		// Remove all Buff Random unit with random Type
		b = new BlessClass(3, 5, 15);
		b.LevelDependency = false;
		b.Level = 1;
		b.Description = "임의의 유닛 모든 저항 제거.";
		b.Effect += () => 
		{
			
			int maxindex = MonsterClass.MonsterInstanceList.Count - 1;
			int index = (int)Random.Range (0, maxindex);
			if (index == maxindex)
			{
				PlayerClass.PCDataInstance.ImmuneType = DamageType.N;
			}
			else
			{
				MonsterClass.MonsterInstanceList [index].ImmuneType = DamageType.N;
			}
		};
		_allBlessPool.Add (b);

		// Remove One Buff Everyunit with type
		b = new BlessClass(1, 3, 10);
		b.LevelDependency = false;
		b.Level = 1;
		b.Description = "모든 유닛의 속성 저항 하나 제거.";
		b.Effect += () =>
		{
			
			DamageType newImmune = (DamageType)(1 << (int)Random.Range(0,4));
			PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType & ~newImmune);
			foreach(MonsterClass m in MonsterClass.MonsterInstanceList)
			{
				m.ImmuneType = (DamageType)(m.ImmuneType & ~newImmune) ;
			}
		};
		_allBlessPool.Add (b);

		// Remove All Buff Everyunit with type
		b = new BlessClass(3, 5, 10);
		b.LevelDependency = false;
		b.Level = 1;
		b.Description = "모든 유닛의 모든 속성 저항 제거.";
		b.Effect += () =>
		{
			PlayerClass.PCDataInstance.ImmuneType = DamageType.N;
			foreach(MonsterClass m in MonsterClass.MonsterInstanceList)
			{
				m.ImmuneType = DamageType.N;
			}
		};
		_allBlessPool.Add (b);

		// Remove buff Player 
		b = new BlessClass(1, 5, 10);
		b.Level = 1;
		b.LevelDependency = false;
		b.Description = "플레이어의 속성 저항 하나 제거.";
		b.Effect += () =>
		{
			
			DamageType newImmune = (DamageType)(1 << (int)Random.Range(0, 4));
			PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType & ~newImmune);
		};
		_allBlessPool.Add(b);

		// Kill Random Unit
		b = new BlessClass(1, 5, 10);
		b.LevelDependency = false;
		b.Level = 1;
		b.Description = "임의의 유닛 즉사.";
		b.Effect += () =>
		{
			
			int maxindex = MonsterClass.MonsterInstanceList.Count - 1;
			int index = (int)Random.Range (0, maxindex);
			if (index == maxindex)
			{
				if (!PlayerClass.PCDataInstance.OneKillImmune)
					PlayerClass.PCDataInstance.hp = 0;;
			}
			else
			{
				if (!MonsterClass.MonsterInstanceList [index].OneKillImmune)
					MonsterClass.MonsterInstanceList [index].hp = 0;
			}
		};
		_allBlessPool.Add(b);

		// Kill Random Enemy
		b = new BlessClass(1, 5, 10);
		b.LevelDependency = false;
		b.Level = 1;
		b.Description = "임의의 적 유닛 즉사.";
		b.Effect += () =>
		{
			
			int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
			if (!MonsterClass.MonsterInstanceList [index].OneKillImmune)
				MonsterClass.MonsterInstanceList [index].hp = 0;
		};
		_allBlessPool.Add(b);

		// Remove Random item from player's Inventory.
		b = new BlessClass(2, 4, 10);
		b.LevelDependency = false;
		b.Level = 1;
		b.Description = "인벤토리에서 임의의 아이템 제거.";
		b.Effect += () =>
		{
			
			int index = (int)Random.Range (0, PlayerClass.PCDataInstance.Inventory.Count);
			PlayerClass.PCDataInstance.Inventory.RemoveAt (index);
		};
		_allBlessPool.Add (b);

		// Decrease durability of wepon/armor 3 * level
		b = new BlessClass(1, 5, 15);
		b.Level = 1;
		b.LevelDependency = false;
		b.Description = "무기와 방어구 중 임의로 내구도 감소.";
		b.Effect += () =>
		{
			
			int index = (int)Random.Range(0,1);
			if (index == 0)
				PlayerClass.PCDataInstance.Weapon.Durability -= 3 * b.Level;
			else
				PlayerClass.PCDataInstance.Armor.Durability -= 3 * b.Level;
		};
	}

	public static List<BlessClass> InitialBlessList()
	{
		int lvl = 1;
		if (lvl <= 0 || lvl > 5)
		{
			Debug.LogAssertion("Wrong lvl range");
			return null;
		}
		List<BlessClass> filtered = AllBlessPool.FindAll(obj => obj.IsValidLevel(lvl));
		int totalWeight = filtered.Sum(obj => obj.Weight);

		List<BlessClass> Picked = new List<BlessClass>();
		
		for (int i = 0; i < 5; i++)
		{
			int t = totalWeight;
			foreach (var bless in filtered)
			{
				if ((int)Random.Range(0, t) < bless.Weight)
				{
					Picked.Add(bless);
					// 만약 중복을 허용하지 않으면
					// filtered.Remove(bless); totalWeight -= bless.Weight
					break;
				}
				t -= bless.Weight;
			}
		}

		return Picked;
	}

    public static List<BlessClass> AllBlessPool
    {
        get
        {
            if(_allBlessPool.Count == 0)
            {
                PoolInitialize();
            }
            return _allBlessPool;
        }
    }

    public static List<BlessClass> RandomBless(int lvl)
    {
        if (lvl <= 0 || lvl > 5)
        {
            Debug.LogAssertion("Wrong lvl range");
            return null;
        }
        List<BlessClass> filtered = AllBlessPool.FindAll(obj => obj.IsValidLevel(lvl));
        int totalWeight = filtered.Sum(obj => obj.Weight);

        List<BlessClass> Picked = new List<BlessClass>();
        
        for (int i = 0; i < 3; i++)
        {
            int t = totalWeight;
            foreach (var bless in filtered)
            {
                if ((int)Random.Range(0, t) < bless.Weight)
                {
                    Picked.Add(bless);
                    // 만약 중복을 허용하지 않으면
                    // filtered.Remove(bless); totalWeight -= bless.Weight
                    break;
                }
                t -= bless.Weight;
            }
        }

        return Picked;
    }
}

