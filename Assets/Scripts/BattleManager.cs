﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour
{
    BattleClass.BattleData Dungeon;
	public MaintenanceManager Maintenance;
    PlayerClass.PCData Player { get { return PlayerClass.PCDataInstance; } }
    public int CurrentRound; // 몇 번째 전투인가
    List<MonsterClass> MonsterList;
    List<BlessClass> LootedBlessList;
    enum SceneStatus { Idle, Battle, Maintenance, End };
    SceneStatus CurrentScene;
	UIActionState CurrentUIState;
	GameObject Selector;
    GameObject ItemSelector;
    GameObject MaintenanceMenu;
    GameObject PrayEffectText;
    GameObject MouseOverText;
    GameObject BlessShowList;
    GameObject LootedBlessShowList;
    GameObject CloseButton;
    GameObject MainScene;
    private DisplayManager dm;
    float PrayEffectTime = 1.25f;
    float PrayEffectTimeMax = 1.25f;
    int removedBlessIndex = -1;
	bool MaintanenceEnd;

    void Start()
    {
		Selector =  GameObject.Find("Select Action");
        ItemSelector = GameObject.Find("Select Item Action");
        ItemSelector.SetActive(false);
        MaintenanceMenu = GameObject.Find("MaintenanceMenu");
        MaintenanceMenu.SetActive(false);
        PrayEffectText = GameObject.Find("PrayEffectText");
        PrayEffectText.SetActive(false);
        BlessShowList = GameObject.Find("BlessShowList");
        BlessShowList.SetActive(false);
        LootedBlessShowList = GameObject.Find("LootedBlessShowList");
        LootedBlessShowList.SetActive(false);
        CloseButton = GameObject.Find("CloseButton");
        CloseButton.SetActive(false);
        MouseOverText = GameObject.Find("MouseOverText");
        MouseOverText.SetActive(false);
        MainScene = GameObject.Find("MainScene");
        MainScene.SetActive(true);

        MonsterClass.MonsterInstanceList = new List<MonsterClass>();
        dm = gameObject.GetComponent<DisplayManager>();
        BlessClass.PoolInitialize ();
        CurrentScene = SceneStatus.Idle;
		CurrentUIState = UIActionState.IDLE;
        new PlayerClass.PCData();
        CurrentRound = 1;
        MonsterGenerator(CurrentRound);
        Dungeon = new BattleClass.BattleData();
		Maintenance = new MaintenanceManager ();
        dm.SetMM(Maintenance);
        Maintenance.DecideDropItems(MonsterClass.MonsterInstanceList);
        CurrentScene = SceneStatus.Battle;
    }

    void Update()
    {
		if (CurrentScene == SceneStatus.Idle)
		{
			//Maintenance.DecideDropItems (MonsterClass.MonsterInstanceList);
		}
        else if (CurrentScene == SceneStatus.Battle)
        {
            if(CurrentUIState == UIActionState.BlessUsing)
            {
                PrayEffectTime = PrayEffectTime - Time.deltaTime;
                if(PrayEffectTime <= 0)
                {
                    CurrentUIState = UIActionState.IDLE;
                    PrayEffectText.SetActive(false);
                    Selector.SetActive(true);
                }
            }
            bool t = Dungeon.DungeonLoop ();
			dm.UpdateEnemyDisplay();
            dm.UpdatePlayerDisplay();
			if (t) {
				if (Dungeon.IsClear ()) {
					CurrentRound++;
                    if (CurrentRound > 20)
                        Debug.Log("Game Finish");
                    else
                    {
                        CurrentUIState = UIActionState.IDLE;
                        CurrentScene = SceneStatus.Maintenance;
                        dm.UpdateLoots();
                        PrayEffectText.SetActive(false);
                        MaintenanceMenu.SetActive(true);
                        Selector.SetActive(false);
                        ItemSelector.SetActive(true);
                        BlessShowList.SetActive(true);
                        TextMesh[] blessTextList = BlessShowList.GetComponentsInChildren<TextMesh>();
                        for (int i = 0; i < 5; i++)
                        {
                            if (i == removedBlessIndex)
                                blessTextList[i].text = "*버려짐*";
                            else
                                blessTextList[i].text = PlayerClass.PCDataInstance.BlessList[i].GetInfo();
                        }
                    }
				}
			}
			else
			{
				CurrentScene = SceneStatus.End;
				Debug.Log ("Game Over");
			}

        }
        else if (CurrentScene == SceneStatus.Maintenance)
        {
            TextMesh[] blessTextList = BlessShowList.GetComponentsInChildren<TextMesh>();
            for (int i = 0; i < 5; i++)
            {
                if (i == removedBlessIndex)
                    blessTextList[i].text = "*버려짐*";
                else
                    blessTextList[i].text = PlayerClass.PCDataInstance.BlessList[i].GetInfo();
            }
        }
    }

    private void _monsterGenerator(int l1, int l2, int l3, int l4, int l5, int mb, int fb)
    {
        for (int i = 0; i < l1; i++)
        {
            new MonsterClass(1);
        }
        for (int i = 0; i < l2; i++)
        {
            new MonsterClass(2);
        }
        for (int i = 0; i < l3; i++)
        {
            new MonsterClass(3);
        }
        for (int i = 0; i < l4; i++)
        {
            new MonsterClass(4);
        }
        for (int i = 0; i < l5; i++)
        {
            new MonsterClass(5);
        }
        for (int i = 0; i < mb; i++)
        {
            new MonsterClass(10, MonsterType.Golem);
        }
        for (int i = 0; i < fb; i++)
        {
            new MonsterClass(10, MonsterType.Dragon);
        }
    }

    private void MonsterGenerator(int round)
    {
        // List<MonsterClass> l = new List<MonsterClass> ();

        int monsterCount = 0;
        for(int i = monsterCount - 1; i >= 0 ; i--)
        {
            MonsterClass.MonsterInstanceList.RemoveAt(i);
        }
        // return l;
        switch (round)
        {
            case 01: _monsterGenerator(2, 0, 0, 0, 0, 0, 0); break;
            case 02: _monsterGenerator(3, 0, 0, 0, 0, 0, 0); break;
            case 03: _monsterGenerator(4, 0, 0, 0, 0, 0, 0); break;
            case 04: _monsterGenerator(3, 1, 0, 0, 0, 0, 0); break;
            case 05: _monsterGenerator(2, 2, 0, 0, 0, 0, 0); break;
            case 06: _monsterGenerator(1, 3, 0, 0, 0, 0, 0); break;
            case 07: _monsterGenerator(0, 4, 0, 0, 0, 0, 0); break;
            case 08: _monsterGenerator(0, 3, 1, 0, 0, 0, 0); break;
            case 09: _monsterGenerator(0, 2, 2, 0, 0, 0, 0); break;
            case 10: _monsterGenerator(0, 0, 0, 0, 0, 1, 0); break;
            case 11: _monsterGenerator(0, 0, 4, 0, 0, 0, 0); break;
            case 12: _monsterGenerator(0, 0, 3, 1, 0, 0, 0); break;
            case 13: _monsterGenerator(0, 0, 2, 2, 0, 0, 0); break;
            case 14: _monsterGenerator(0, 0, 1, 3, 0, 0, 0); break;
            case 15: _monsterGenerator(0, 0, 0, 4, 0, 0, 0); break;
            case 16: _monsterGenerator(0, 0, 0, 3, 1, 0, 0); break;
            case 17: _monsterGenerator(0, 0, 0, 2, 2, 0, 0); break;
            case 18: _monsterGenerator(0, 0, 0, 1, 3, 0, 0); break;
            case 19: _monsterGenerator(0, 0, 0, 0, 4, 0, 0); break;
            case 20: _monsterGenerator(0, 0, 0, 0, 0, 0, 1); break;
        }
        dm.InitializeEnemyDisplay();
        dm.UpdatePlayerDisplay();
    }

	public void ManageButtonClick(string buttonName)
    {
        MainScene.SetActive(false);
        if (CurrentScene == SceneStatus.End)
        {
            return;
        }
		if (buttonName == "Attack Button")
		{
			if (CurrentUIState == UIActionState.IDLE) 
			{
                PrayEffectText.SetActive(true);
                PrayEffectText.GetComponentInChildren<TextMesh>().text = "공격 대상을 선택하세요.";
                CurrentUIState = UIActionState.EnemySelecting;
				Selector.SetActive (false);
			} 
			else 
			{
				Debug.Log ("Wrong Input");
			}
		}
		else if (buttonName == "Pray Button")
		{
			if (CurrentUIState == UIActionState.IDLE) 
			{
                CurrentUIState = UIActionState.BlessUsing;
                PrayEffectTime = PrayEffectTimeMax;
                PrayEffectText.SetActive(true);
                Selector.SetActive (false);
				Dungeon.Action (1);
            } 
			else
			{
				Debug.Log ("Wrong Input");
			}
		}
		else if (buttonName == "Item Use Button")
		{
			if (CurrentUIState == UIActionState.IDLE) 
			{
				CurrentUIState = UIActionState.ItemSelecting;
				Selector.SetActive (false);
                PrayEffectText.GetComponentInChildren<TextMesh>().text = "사용하거나 장비할 아이템을 선택하세요.";
                PrayEffectText.SetActive(true);
			} 
			else 
			{
				Debug.Log ("Wrong Input");
			}
		}
        else if (buttonName == "Item Use 2 Button")
        {
            if (CurrentUIState == UIActionState.IDLE)
            {
                CurrentUIState = UIActionState.ItemSelecting2;
                ItemSelector.SetActive(false);
                PrayEffectText.GetComponentInChildren<TextMesh>().text = "사용하거나 장비할 아이템을 선택하세요.";
                PrayEffectText.SetActive(true);
            }
            else
            {
                Debug.Log("Wrong Input");
            }
        }
        else if (buttonName == "Bless Check Button") 
		{
            if (CurrentUIState == UIActionState.IDLE)
            {
                CurrentUIState = UIActionState.BlessChecking;
                Selector.SetActive(false);
                CloseButton.SetActive(true);
                BlessShowList.SetActive(true);
                TextMesh[] blessTextList = BlessShowList.GetComponentsInChildren<TextMesh>();
                for (int i = 0; i < 5; i++)
                {
					blessTextList[i].text = PlayerClass.PCDataInstance.BlessList[i].GetInfo();
                }
            } 
			else 
			{
				Debug.Log ("Wrong Input");
			}
		}
		else if (buttonName.Contains ("enemy")) 
		{
            if (CurrentUIState == UIActionState.EnemySelecting)
            {
                int EnemyNum = int.Parse(buttonName.Substring(5)) - 1;
                int EnemyIdx = 0;
                for (int i = 0; i < MonsterClass.MonsterInstanceList.Count; i++)
                {
                    if (MonsterClass.MonsterInstanceList[i].ListIndex == EnemyNum)
                    {
                        EnemyIdx = i;
                        break;
                    }
                }
				Dungeon.Action (0, MonsterClass.MonsterInstanceList [EnemyIdx]);
				CurrentUIState = UIActionState.IDLE;
				Selector.SetActive (true);
                PrayEffectText.SetActive(false);
			}
		}
        else if (buttonName == "CloseButton")
        {
            if (CurrentUIState == UIActionState.BlessChecking)
            {
                CurrentUIState = UIActionState.IDLE;
                CloseButton.SetActive(false);
                BlessShowList.SetActive(false);
                Selector.SetActive(true);
            }
        }
        else if (buttonName == "Close") //expired
		{
			if (CurrentUIState == UIActionState.EnemySelecting)
				CurrentUIState = UIActionState.IDLE;
		} 
		else if (buttonName == "Close On Item Target") 
		{
			if (CurrentUIState == UIActionState.EnemySelecting)
				CurrentUIState = UIActionState.ItemSelecting;
		}
        else if (buttonName.Contains("bless_"))
        {
            if (CurrentUIState == UIActionState.SelectingBless)
            {
                CurrentUIState = UIActionState.IDLE;
                int ItemNum = int.Parse(buttonName.Substring(6)) - 1;
                PlayerClass.PCDataInstance.BlessList[removedBlessIndex] = LootedBlessList[ItemNum];
                LootedBlessList = null;
                removedBlessIndex = -1;
                PrayEffectText.SetActive(false);
                MaintenanceMenu.SetActive(true);
                LootedBlessShowList.SetActive(false);
                ItemSelector.SetActive(true);
            }
        }
        else if (buttonName.Contains ("slot") && CurrentUIState == UIActionState.ItemSelecting) 
		{
			Debug.Log (1);
			if (CurrentUIState == UIActionState.ItemSelecting)
			{
				Debug.Log(2);
				int ItemNum = int.Parse(buttonName.Substring (4)) - 1;
				if (Player.UsingItem (ItemNum) == 1) {
					Dungeon.Action (2, ItemNum);
					CurrentUIState = UIActionState.IDLE;
					Selector.SetActive (true);

                } else if (Player.UsingItem (ItemNum) == 0) {
					if (PlayerClass.PCDataInstance.Inventory [ItemNum].IType == ItemType.Weapon) {
						WeaponClass temp = Player.Weapon;
						Player.Weapon = (WeaponClass)PlayerClass.PCDataInstance.Inventory [ItemNum];
                        if (temp != null)
                        {
                            PlayerClass.PCDataInstance.Inventory[ItemNum] = (ItemClass)temp;
                        }
                        else
                        {
                            PlayerClass.PCDataInstance.Inventory.RemoveAt(ItemNum);
                        }
                    } else if (PlayerClass.PCDataInstance.Inventory [ItemNum].IType == ItemType.Armor) {
						ArmorClass temp = Player.Armor;
						Player.Armor = (ArmorClass)PlayerClass.PCDataInstance.Inventory [ItemNum];
                        if (temp != null)
                        {
                            PlayerClass.PCDataInstance.Inventory[ItemNum] = (ItemClass)temp;
                        }
                        else
                        {
                            PlayerClass.PCDataInstance.Inventory.RemoveAt(ItemNum);
                        }
                    }
					CurrentUIState = UIActionState.IDLE;
					Selector.SetActive (true);
				} else {
					CurrentUIState = UIActionState.IDLE;
					Selector.SetActive (true);
				}
                PrayEffectText.SetActive(false);
            }
		}
        else if (buttonName.Contains("slot") && CurrentUIState == UIActionState.ItemSelecting2)
        {
            Debug.Log(1);
            if (CurrentUIState == UIActionState.ItemSelecting2)
            {
                Debug.Log(2);
                int ItemNum = int.Parse(buttonName.Substring(4)) - 1;
                if (Player.UsingItem(ItemNum) == 1)
                {
                    ConsumableClass I = (ConsumableClass)PlayerClass.PCDataInstance.Inventory[ItemNum];
                    I.ItemEffect();
                    Player.Inventory.RemoveAt(ItemNum);
                    CurrentUIState = UIActionState.IDLE;
                    ItemSelector.SetActive(true);
                }
                else if (Player.UsingItem(ItemNum) == 0)
                {
                    if (PlayerClass.PCDataInstance.Inventory[ItemNum].IType == ItemType.Weapon)
                    {
                        WeaponClass temp = Player.Weapon;
                        Player.Weapon = (WeaponClass)PlayerClass.PCDataInstance.Inventory[ItemNum];
                        if (temp != null)
                        {
                            PlayerClass.PCDataInstance.Inventory[ItemNum] = (ItemClass)temp;
                        }
                        else
                        {
                            PlayerClass.PCDataInstance.Inventory.RemoveAt(ItemNum);
                        }
                    }
                    else if (PlayerClass.PCDataInstance.Inventory[ItemNum].IType == ItemType.Armor)
                    {
                        ArmorClass temp = Player.Armor;
                        Player.Armor = (ArmorClass)PlayerClass.PCDataInstance.Inventory[ItemNum];
                        if (temp != null)
                        {
                            PlayerClass.PCDataInstance.Inventory[ItemNum] = (ItemClass)temp;
                        }
                        else
                        {
                            PlayerClass.PCDataInstance.Inventory.RemoveAt(ItemNum);
                        }
                    }
                    CurrentUIState = UIActionState.IDLE;
                    ItemSelector.SetActive(true);
                }
                else
                {
                    CurrentUIState = UIActionState.IDLE;
                    ItemSelector.SetActive(true);
                }
                dm.UpdatePlayerDisplay();
                PrayEffectText.SetActive(false);
            }
        }
        else if (buttonName.Contains("looted_item") || buttonName.Contains("slot"))
        {
            Debug.Log(buttonName);
            int ItemNum;
            bool isInInventory = buttonName.Contains("slot");
            if(isInInventory)
                ItemNum = int.Parse(buttonName.Substring(4)) - 1;
            else
                ItemNum = int.Parse(buttonName.Substring(11)) - 1;
            if (CurrentUIState == UIActionState.Sacrificing)
            {
				if (!isInInventory) {
					if (ItemNum >= Maintenance.LootingItems.Count)
						return;
				} else {
					if (ItemNum >= Player.Inventory.Count)
						return;
				}
				// Check Buuton name and remove it from rewards
                removedBlessIndex = Random.Range(0, 5);
                if (!isInInventory)
                    LootedBlessList = BlessClass.RandomBless(Maintenance.LootingItems[ItemNum].ItemLevel);
                else
                    LootedBlessList = BlessClass.RandomBless(Player.Inventory[ItemNum].ItemLevel);
                // item Level here
                LootedBlessShowList.SetActive(true);
                TextMesh[] blessTextList = LootedBlessShowList.GetComponentsInChildren<TextMesh>();
                for (int i = 0; i < 3; i++)
                {
                    blessTextList[i].text = LootedBlessList[i].GetInfo();
                }
                CurrentUIState = UIActionState.SelectingBless;
                PrayEffectText.GetComponentInChildren<TextMesh>().text = "선호하는 축복을 선택하세요.";
                MaintenanceMenu.SetActive(false);

				if (!isInInventory) {
					if (ItemNum < Maintenance.LootingItems.Count)
						Maintenance.LootingItems.RemoveAt (ItemNum);
				} else {
					if (ItemNum < Player.Inventory.Count)
						Player.Inventory.RemoveAt (ItemNum);
				}
            }
            else if (CurrentUIState == UIActionState.Discarding)
            {
				if (!isInInventory) {
					if (ItemNum >= Maintenance.LootingItems.Count)
						return;
				} else {
					if (ItemNum >= Player.Inventory.Count)
						return;
				}
				PrayEffectText.SetActive(false);
				if (!isInInventory)
					Maintenance.LootingItems.RemoveAt (ItemNum);
				else
					Player.Inventory.RemoveAt (ItemNum);
                CurrentUIState = UIActionState.IDLE;
                ItemSelector.SetActive (true);
            }
            else if (CurrentUIState == UIActionState.Looting && !isInInventory)
            {
				if (!isInInventory) {
					if (ItemNum >= Maintenance.LootingItems.Count)
						return;
				} else {
					if (ItemNum >= Player.Inventory.Count)
						return;
				}
                PrayEffectText.SetActive(false);
				Player.Inventory.Add (Maintenance.LootingItems [ItemNum]);
				Maintenance.LootingItems.RemoveAt (ItemNum);
                CurrentUIState = UIActionState.IDLE;
                ItemSelector.SetActive (true);
            }
            dm.UpdateLoots();
            dm.UpdateInventories();
        }
        else if (buttonName == "Sacrifice Button")
        {
            if (CurrentUIState == UIActionState.IDLE)
            {
                CurrentUIState = UIActionState.Sacrificing;
                PrayEffectText.SetActive(true);
                PrayEffectText.GetComponentInChildren<TextMesh>().text = "신에게 바칠 아이템을 선택하세요.";
                ItemSelector.SetActive (false);
            }
        }
        else if (buttonName == "Discard Button")
        {
            if (CurrentUIState == UIActionState.IDLE)
            {
                CurrentUIState = UIActionState.Discarding;
                PrayEffectText.SetActive(true);
                PrayEffectText.GetComponentInChildren<TextMesh>().text = "버릴 아이템을 선택하세요.";
                ItemSelector.SetActive (false);
            }
        }
        else if (buttonName == "Loot Button")
        {
            if (CurrentUIState == UIActionState.IDLE)
            {
                CurrentUIState = UIActionState.Looting;
                PrayEffectText.SetActive(true);
                PrayEffectText.GetComponentInChildren<TextMesh>().text = "가방에 넣을 아이템을 선택하세요.";
                ItemSelector.SetActive (false);
            }
        }
        else if (buttonName == "Next Button")
        {
            if (CurrentUIState == UIActionState.IDLE)
            {
                CurrentScene = SceneStatus.Battle;
                PrayEffectText.SetActive(false);
                MaintenanceMenu.SetActive(false);
                Selector.SetActive(true);
                ItemSelector.SetActive(false);
                MonsterGenerator(CurrentRound);
                Maintenance.DecideDropItems(MonsterClass.MonsterInstanceList);
                BlessShowList.SetActive(false);
                Player.hp = Player.hp + 30;
                if (Player.hp > Player.maxhp) Player.hp = Player.maxhp;
            }
        }
        dm.UpdateLoots();
		dm.UpdateInventories();
    }

    public void ManageMouseOver(string buttonName)
    {
        if(buttonName == "weaponslot" )
        {
            WeaponClass Item = PlayerClass.PCDataInstance.Weapon;
            if (Item != null)
            {
                MouseOverText.SetActive(true);
                MouseOverText.GetComponentInChildren<TextMesh>().text = ((WeaponClass)Item).GetInfo();
            }
        }
        else if ( buttonName == "armorslot")
        {
            ArmorClass Item = PlayerClass.PCDataInstance.Armor;
            if (Item != null)
            {
                MouseOverText.SetActive(true);
                MouseOverText.GetComponentInChildren<TextMesh>().text = ((ArmorClass)Item).GetInfo();
            }
        }
        else if (buttonName.Contains("looted_item") || buttonName.Contains("slot"))
        {
            int ItemNum;
            bool isInInventory = buttonName.Contains("slot");
            bool isEmpty;
            if (isInInventory)
            {
                ItemNum = int.Parse(buttonName.Substring(4)) - 1;
                isEmpty = (ItemNum >= PlayerClass.PCDataInstance.Inventory.Count);
                if (!isEmpty && PlayerClass.PCDataInstance.Inventory[ItemNum] == null)
                    isEmpty = true;
            }
            else
            {
                ItemNum = int.Parse(buttonName.Substring(11)) - 1;
                isEmpty = ItemNum >= Maintenance.LootingItems.Count;
                if (!isEmpty && Maintenance.LootingItems[ItemNum] == null)
                    isEmpty = true;
            }

            if (!isEmpty)
            {
                ItemClass Item;
                if (!isInInventory)
                    Item = Maintenance.LootingItems[ItemNum];
                else
                    Item = Player.Inventory[ItemNum];

                MouseOverText.SetActive(true);
                if (Item.IType == ItemType.Weapon)
                    MouseOverText.GetComponentInChildren<TextMesh>().text = ((WeaponClass)Item).GetInfo();
                else if (Item.IType == ItemType.Armor)
                    MouseOverText.GetComponentInChildren<TextMesh>().text = ((ArmorClass)Item).GetInfo();
                else if (Item.IType == ItemType.Consumable)
                    MouseOverText.GetComponentInChildren<TextMesh>().text = ((ConsumableClass)Item).GetInfo();
            }

       /*
       if (buttonName.Contains("slot"))
        {
            int ItemNum;
            ItemNum = int.Parse(buttonName.Substring(4)) - 1;
            
                MouseOverText.GetComponentInChildren<TextMesh>().text = "비어있음";
            else
                MouseOverText.GetComponentInChildren<TextMesh>().text = "뭔가있음";
            MouseOverText.SetActive(true);
        }
        else if(buttonName.Contains("looted_item"))
        {
            int ItemNum;
            ItemNum = int.Parse(buttonName.Substring(11)) - 1;
            if ()
                MouseOverText.GetComponentInChildren<TextMesh>().text = "비어있음";
            else
                MouseOverText.GetComponentInChildren<TextMesh>().text = "뭔가있음";
            MouseOverText.SetActive(true);
        */
        }
    }
    public void ManageMouseExit()
    {
        MouseOverText.SetActive(false);
    }
}

public enum UIActionState { IDLE, EnemySelecting, ItemEnemySelecting, ItemSelecting, BlessChecking, BlessUsing, Sacrificing, Discarding, Looting, SelectingBless, ItemSelecting2};

[System.Flags]
public enum DamageType
{
    N = 0x00,
    T1 = 0x01,
    T2 = 0x02,
    T3 = 0x04,
    T4 = 0x08,
    A = 0x0F
}