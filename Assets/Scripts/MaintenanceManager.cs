﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaintenanceManager : MonoBehaviour
{
	PlayerClass.PCData _pcdata;

	List<ItemClass> Inventory
	{
		get
		{
			return _pcdata.Inventory;
		}
	}
	public List<ItemClass> LootingItems = new List<ItemClass>();

	public void DecideDropItems(List<MonsterClass> m)
	{
		foreach (MonsterClass M in m)
		{
			int Weapon_Consume = (int)Random.Range (0, 100);
			int ItemLevel = (int)Random.Range (0, 100);
			int Weapon_type = (int)Random.Range (0, 5);
			int Armor_type = (int)Random.Range (0, 4);
			int Cons_type = (int)Random.Range (0, 10);
			int l;
			switch (M.Level)
			{
            case 1:
                if (Weapon_Consume <= 15)
                {
                    WeaponClass item = new WeaponClass((WeaponType)Weapon_type, 1);
                    LootingItems.Add(item);
                }
                else if (Weapon_Consume <= 30)
                {
                    ArmorClass item = new ArmorClass((ArmorType)Armor_type, 1);
                    LootingItems.Add(item);
                }
                else
                {
                    ConsumableClass item = new ConsumableClass(1);
                    LootingItems.Add(item);
				}
				break;
			case 2:
				if (ItemLevel <= 60)
					l = 1;
				else
					l = 2;
				if (Weapon_Consume <= 15) {
					WeaponClass item = new WeaponClass ((WeaponType)Weapon_type, l);
					LootingItems.Add (item);
				} else if (Weapon_Consume <= 30) {
					ArmorClass item = new ArmorClass ((ArmorType)Armor_type, l);
					LootingItems.Add (item);
				} else {
					ConsumableClass item = new ConsumableClass (l);
					LootingItems.Add (item);
				}
				break;
			case 3:
				if (ItemLevel <= 25)
					l = 1;
				else if (ItemLevel <= 75)
					l = 2;
				else
					l = 3;
				if (Weapon_Consume <= 15) {
					WeaponClass item = new WeaponClass ((WeaponType)Weapon_type, l);
					LootingItems.Add (item);
				} else if (Weapon_Consume <= 30) {
					ArmorClass item = new ArmorClass ((ArmorType)Armor_type, l);
					LootingItems.Add (item);
				} else {
					ConsumableClass item = new ConsumableClass (l);
					LootingItems.Add (item);
				}
				break;
			case 4:
				for (int i = 0; i < 2; i++) {
					if (ItemLevel <= 25)
						l = 2;
					else if (ItemLevel <= 75)
						l = 3;
					else
						l = 4;
					if (Weapon_Consume <= 15) {
						WeaponClass item = new WeaponClass ((WeaponType)Weapon_type, l);
						LootingItems.Add (item);
					} else if (Weapon_Consume <= 30) {
						ArmorClass item = new ArmorClass ((ArmorType)Armor_type, l);
						LootingItems.Add (item);
					} else {
						ConsumableClass item = new ConsumableClass (l);
						LootingItems.Add (item);
					}
				}
				break;
			case 5:
				for (int i = 0; i < 3; i++) {
					if (ItemLevel <= 25)
						l = 3;
					else if (ItemLevel <= 75)
						l = 4;
					else
						l = 5;
					if (Weapon_Consume <= 15) {
						WeaponClass item = new WeaponClass ((WeaponType)Weapon_type, l);
						LootingItems.Add (item);
					} else if (Weapon_Consume <= 30) {
						ArmorClass item = new ArmorClass ((ArmorType)Armor_type, l);
						LootingItems.Add (item);
					} else {
						ConsumableClass item = new ConsumableClass (l);
						LootingItems.Add (item);
					}
				}
				break;
			}
		}
	}

	public void UsingItem(ItemClass item)
	{
		switch (item.IType)
		{
		case ItemType.Weapon:
		case ItemType.Armor:
			Debug.LogAssertion("Cannot consum Equipment");
			return;
		case ItemType.Consumable:
			((ConsumableClass)item).ItemEffect ();
			// TODO: 사용할 수 있는 상황이라면 해당 효과 발동
			break;
		default:
			break;
		}
	}

	public void SettingItem(ItemClass item)
	{
		ItemClass p_item = null;
		List<ItemClass> iList;
		if (Inventory.Contains(item))
		{
			iList = Inventory;
		}
		else if (LootingItems.Contains(item))
		{
			iList = LootingItems;
		}
		else
		{
			Debug.LogAssertion("No item to equip");
			return;
		}
		switch (item.IType)
		{
		case ItemType.Weapon:
			p_item = _pcdata.Weapon;
			_pcdata.Weapon = (WeaponClass)item;
			break;
		case ItemType.Armor:
			p_item = _pcdata.Armor;
			_pcdata.Armor = (ArmorClass)item;
			break;
		case ItemType.Consumable:
			Debug.LogAssertion("Consumable item cannot equip");
			return;
		default:
			break;
		}

		int i = iList.IndexOf(item);
		iList[i] = p_item;
	}
	public void OfferingItem(ItemClass item)
	{
		if (Inventory.Contains(item))
		{
			int i = Inventory.IndexOf(item);
			Inventory[i] = null;
		}
		else if (LootingItems.Contains(item))
		{
			int i = LootingItems.IndexOf(item);
			LootingItems[i] = null;
		}
		else
		{
			return;
		}
		// TODO
		// int j = EraseBless();
		// 템렙에 따라 축복 풀 구성
		// 풀 에서 3 개 뽑음
		// 플레이어가 3개 중 하나 뽑음
		// 축복 대체
	}

	public void MoveToInventory(ItemClass item)
	{
		int i = Inventory.IndexOf(null);
		if (i < 0) return;
		if (i >= 6) return;

		if (0 <= _deleteFromFloor(item))
		{
			Inventory[i] = item;
		}
	}
	public void MoveToFloor(ItemClass item)
	{
		if (0 <= _deleteFromInventory(item))
		{
			int i = LootingItems.IndexOf(null);
			if (i < 0)
			{
				LootingItems.Add(item);
			}
			else
			{
				LootingItems[i] = item;
			}
		}
	}
	int _deleteFromItemList(ItemClass item, List<ItemClass> iList)
	{
		if (iList.Contains(item))
		{
			int i = iList.IndexOf(item);
			iList[i] = null;
			return i;
		}
		else
		{
			return -1;
		}
	}
	int _deleteFromInventory(ItemClass item)
	{
		return _deleteFromItemList(item, Inventory);
	}
	int _deleteFromFloor(ItemClass item)
	{
		return _deleteFromItemList(item, LootingItems);
	}
}
