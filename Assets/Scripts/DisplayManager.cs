﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayManager: MonoBehaviour {

    public Sprite[] monsterTextures;
    public Sprite[] itemTextures;
    public Sprite[] attackTypeTextures;


    public Transform[] enemies;
    private bool[] hasEnemy = new bool[5];
    private SpriteRenderer[] e_sprite = new SpriteRenderer[5];
    private TextMesh[] e_name = new TextMesh[5];
    private TextMesh[] e_hp = new TextMesh[5];
    private SpriteRenderer[,] e_immune = new SpriteRenderer[5, 4];
    private SpriteRenderer[,] e_attack = new SpriteRenderer[5, 4];

    public Transform[] inventories;
    private SpriteRenderer[] i_slots=new SpriteRenderer[6];
    private SpriteRenderer[] i_items=new SpriteRenderer[6];
    public Transform[] equipments;
    private SpriteRenderer[] e_slots=new SpriteRenderer[2];
    private SpriteRenderer[] e_items=new SpriteRenderer[2];

    public Transform player;
    private TextMesh p_dmg = new TextMesh();
    private TextMesh p_armor = new TextMesh();
    private TextMesh p_hp = new TextMesh();
    private SpriteRenderer[] p_immune = new SpriteRenderer[4];
    private SpriteRenderer p_attack = new SpriteRenderer();

    public Transform[] loots;
    private SpriteRenderer[] l_items = new SpriteRenderer[12];

    private BattleManager bm;
    private MaintenanceManager mm;
	// Use this for initialization
	void Start ()
    {
        bm = gameObject.GetComponent<BattleManager>();
        for (int i = 0; i < 5; i++)
        {
            e_sprite[i] = enemies[i].Find("MainTexture").GetComponent<SpriteRenderer>();
            e_name[i] = enemies[i].Find("name").GetComponent<TextMesh>();
            e_hp[i] = enemies[i].Find("HP").GetComponent<TextMesh>();
            for(int j=0;j<4;j++)
            {
                e_immune[i, j] = enemies[i].Find("immune" + (j+1)).GetComponent<SpriteRenderer>();
                e_attack[i, j] = enemies[i].Find("attack" + (j+1)).GetComponent<SpriteRenderer>();
            }
        }

        for (int i = 0; i < 6; i++)
        {
            i_slots[i] = inventories[i].gameObject.GetComponent<SpriteRenderer>();
            i_items[i] = inventories[i].Find("item" + (i + 1)).GetComponent<SpriteRenderer>();
        }
        e_slots[0] = equipments[0].gameObject.GetComponent<SpriteRenderer>();
        e_items[0] = equipments[0].Find("weapon").GetComponent<SpriteRenderer>();
        e_slots[1] = equipments[1].gameObject.GetComponent<SpriteRenderer>();
        e_items[1] = equipments[1].Find("armor").GetComponent<SpriteRenderer>();

        p_dmg = player.Find("atk").GetComponent<TextMesh>();
        p_armor = player.Find("def").GetComponent<TextMesh>();
        p_hp = player.Find("HP").GetComponent<TextMesh>();
        for (int i = 0; i < 4; i++)
        {
            p_immune[i] = player.Find("immune" + (i + 1)).GetComponent<SpriteRenderer>();
        }
        p_attack = player.Find("attacktype").GetComponent<SpriteRenderer>();

        for (int i = 0; i < 12; i++)
        {
            l_items[i] = loots[i].Find("itemSprite").GetComponent<SpriteRenderer>();
        }
	}
    public void SetMM(MaintenanceManager m)
    {
        mm = m;
    }
    public void InitializeEnemyDisplay()
    {
        hasEnemy = new bool[5];
        foreach (MonsterClass enemy in MonsterClass.MonsterInstanceList)
        {
            hasEnemy[enemy.ListIndex] = true;
        }
        for (int i = 0; i < 5; i++)
        {
            enemies[i].gameObject.SetActive(hasEnemy[i]);
        }
        foreach (MonsterClass enemy in MonsterClass.MonsterInstanceList)
        {
            int index = enemy.ListIndex;
            switch (enemy.MonsterT)
            {
                case MonsterType.Slime:
                    e_sprite[index].sprite = monsterTextures[0];
                    e_name[index].text = "Slime";
                    break;
                case MonsterType.Goblin:
                    e_sprite[index].sprite = monsterTextures[1];
                    e_name[index].text = "Goblin";
                    break;
                case MonsterType.Skeleton:
                    e_sprite[index].sprite = monsterTextures[2];
                    e_name[index].text = "Skeleton";
                    break;
                case MonsterType.Spirit:
                    e_sprite[index].sprite = monsterTextures[3];
                    e_name[index].text = "Spirit";
                    break;
                case MonsterType.Demon:
                    e_sprite[index].sprite = monsterTextures[4];
                    e_name[index].text = "Demon";
                    break;
                case MonsterType.Golem:
                    index = 4;
                    enemy.ListIndex = 4;
                    e_sprite[index].sprite = monsterTextures[5];
                    e_name[index].text = "Golem";
                    break;
                case MonsterType.Dragon:
                    index = 4;
                    enemy.ListIndex = 4;
                    e_sprite[index].sprite = monsterTextures[6];
                    e_name[index].text = "Dragon";
                    break;
            }
            e_hp[index].text = enemy.hp + " / " + enemy.maxhp;
            int im = (int)enemy.ImmuneType;
            int dm = (int)enemy.PowerType;
            for (int i = 0; i < 4; i++)
            {  
                if (im % 2 == 1)
                {
                    e_immune[index, i].color = Color.white;
                    im -= 1;
                }
                else
                    e_immune[index, i].color = new Color(1f, 1f, 1f, 0.25f);
                im /= 2;
                if (dm % 2 == 1)
                {
                    e_attack[index, i].color = Color.white;
                    dm -= 1;
                }
                else
                    e_attack[index, i].color = new Color(1f, 1f, 1f, 0.25f);
                dm /= 2;
            }
        }
    }
    public void UpdateEnemyDisplay()
    {
        hasEnemy = new bool[5];
        foreach (MonsterClass enemy in MonsterClass.MonsterInstanceList)
        {
            hasEnemy[enemy.ListIndex] = true;
        }
        for (int i = 0; i < 5; i++)
        {
            enemies[i].gameObject.SetActive(hasEnemy[i]);
        }
        foreach (MonsterClass enemy in MonsterClass.MonsterInstanceList)
        {
            int index = enemy.ListIndex;
            e_hp[index].text = enemy.hp + " / " + enemy.maxhp;
            int im = (int)enemy.ImmuneType;
            int dm = (int)enemy.PowerType;
            for (int i = 0; i < 4; i++)
            {  
                if (im % 2 == 1)
                {
                    e_immune[index, i].color = Color.white;
                    im -= 1;
                }
                else
                    e_immune[index, i].color = new Color(1f, 1f, 1f, 0.25f);
                im /= 2;
                if (dm % 2 == 1)
                {
                    e_attack[index, i].color = Color.white;
                    dm -= 1;
                }
                else
                    e_attack[index, i].color = new Color(1f, 1f, 1f, 0.25f);
                dm /= 2;
            }
        }
    }

    public void UpdatePlayerDisplay()
    {
        UpdatePlayerStat();
        UpdatePlayerEquip();
        UpdateInventories();
    }
    public void UpdatePlayerStat()
    {
        p_hp.text = PlayerClass.PCDataInstance.hp + " / " + PlayerClass.PCDataInstance.maxhp;
        int im = (int)PlayerClass.PCDataInstance.ImmuneType;
        for (int i = 0; i < 4; i++)
        {
            if (im % 2 == 1)
            {
                p_immune[i].color = Color.white;
                im -= 1;
            }
            else
                p_immune[i].color = new Color(1f, 1f, 1f, 0.25f);
            im /= 2;
        }
        int dm = 0;
        if (PlayerClass.PCDataInstance.Weapon != null)
            dm = (int)PlayerClass.PCDataInstance.Weapon.DamageT;
        switch (dm)
        {
            case 1:
                p_attack.sprite = attackTypeTextures[0];
                p_attack.color = Color.white;
                break;
            case 2:
                p_attack.sprite = attackTypeTextures[1];
                p_attack.color = Color.white;
                break;
            case 4:
                p_attack.sprite = attackTypeTextures[2];
                p_attack.color = Color.white;
                break;
            case 8:
                p_attack.sprite = attackTypeTextures[3];
                p_attack.color = Color.white;
                break;
            default:
                p_attack.color = new Color(1f, 1f, 1f, 0f);
                break;
        }
    }
    public void UpdatePlayerEquip()
    {
        if (PlayerClass.PCDataInstance.Weapon == null)
        {
            e_items[0].color = new Color(1f, 1f, 1f, 0f);
            p_dmg.text = "1 ~ 1";
        }
        else
        {
            e_items[0].sprite = itemTextures[(int)PlayerClass.PCDataInstance.Weapon.WeaponT];
            e_items[0].color = Color.white;
            p_dmg.text = PlayerClass.PCDataInstance.Weapon.MinDamage + " ~ " + PlayerClass.PCDataInstance.Weapon.MaxDamage;
        }

        if (PlayerClass.PCDataInstance.Armor == null)
        {
            e_items[1].color = new Color(1f, 1f, 1f, 0f);
            p_armor.text = "0";
        }
        else
        {
            e_items[1].sprite = itemTextures[(int)PlayerClass.PCDataInstance.Armor.ArmorT+5];
            e_items[1].color = Color.white;
            p_armor.text = PlayerClass.PCDataInstance.Armor.Defence.ToString();
        }
    }
    public void UpdateInventories()
    {
        for (int i = 0; i < 6; i++)
        {
            if (i < PlayerClass.PCDataInstance.Inventory.Count)
            {
                if (PlayerClass.PCDataInstance.Inventory[i] == null)
                {
                    i_items[i].color = new Color(1f, 1f, 1f, 0f);
                }
                else
                {
                    i_items[i].sprite = itemTextures[PlayerClass.PCDataInstance.Inventory[i].ItemInfo];
                    i_items[i].color = Color.white;
                }
            }
            else
            {
                i_items[i].color = new Color(1f, 1f, 1f, 0f);
            }
        }
    }
    public void UpdateLoots()
    {
        for (int i = 0; i < 12; i++)
        {
            if(i < mm.LootingItems.Count)
            {
                if (mm.LootingItems[i] == null)
                {
                    l_items[i].color = new Color(1f, 1f, 1f, 0f);
                }
                else
                {
                    l_items[i].sprite = itemTextures[mm.LootingItems[i].ItemInfo];
                    l_items[i].color = Color.white;
                }
            }
            else
            {
                l_items[i].color = new Color(1f, 1f, 1f, 0f);
            }
        }
    }
}
