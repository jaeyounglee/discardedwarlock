﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemClass
{
	public ItemType IType;
	public int MaxDurability;
    public int Durability;
    public int ItemLevel;
    public int ItemInfo;

    public bool IsBroken()
    {
        if (Durability <= 0)
            return true;
        return false;
    }

    public string GetInfo()
    {
        return "";
    }
}

public class WeaponClass : ItemClass
{
    public WeaponType WeaponT;
    public int MinDamage;
    public int MaxDamage;
	public DamageType DamageT;

	public WeaponClass(WeaponType w, int Level)
    {
		ItemLevel = Level;
        ItemInfo = (int)w;
        IType = ItemType.Weapon;
		int t = (int)Random.Range (0, 3);
		int t2 = (int)Random.Range (0, 3);
		int t3 = (int)Random.Range (0, 10);
        switch (w)
        {
		case WeaponType.Ax:
			MinDamage = 4 + 2 * ItemLevel;
			MaxDamage = 8 + 2 * ItemLevel;
			Durability = 25 + 5 * ItemLevel;
			WeaponT = WeaponType.Ax;
            break;
		case WeaponType.Dagger:
			MinDamage = 5 + 2 * ItemLevel;
			MaxDamage = 7 + 2 * ItemLevel;
			Durability = 12 + 3 * ItemLevel;
			WeaponT = WeaponType.Dagger;
            break;
		case WeaponType.Hammer:
			MinDamage = 4 + ItemLevel;
			MaxDamage = 12 + 3 * ItemLevel;
			Durability = 25 + 5 * ItemLevel;
			WeaponT = WeaponType.Hammer;
            break;
		case WeaponType.LongSword:
			MinDamage = 7 + 2 * ItemLevel;
			MaxDamage = 10 + 2 * ItemLevel;
			Durability = 25 + 5 * ItemLevel;
			WeaponT = WeaponType.LongSword;
            break;
		case WeaponType.Spear:
			MinDamage = 4 + 2 * ItemLevel;
			MaxDamage = 8 + 2 * ItemLevel;
			Durability = 25 + 5 * ItemLevel;
			WeaponT = WeaponType.Spear;
            break;
        }
		MinDamage = (int)(MinDamage * (1 - 0.1 * (t - 1)));
		MaxDamage = (int)(MaxDamage * (1 - 0.1 * (t2 - 1)));

		if (t3 <= 3)
		{
			DamageT = (DamageType)(1 << t3);
			MinDamage = (int)(MinDamage * 1.5);
			MaxDamage = (int)(MaxDamage * 1.5);
		}
		MaxDurability = Durability;
    }

	public void ReLevel(int i)
	{
		
		int t = (int)Random.Range (0, 3);
		int t2 = (int)Random.Range (0, 3);
		int t3 = (int)Random.Range (0, 10);
		ItemLevel += i;
		switch (WeaponT)
		{
		case WeaponType.Ax:
			MinDamage = 4 + 2 * ItemLevel;
			MaxDamage = 8 + 2 * ItemLevel;
			Durability = 25 + 5 * ItemLevel;
			break;
		case WeaponType.Dagger:
			MinDamage = 5 + 2 * ItemLevel;
			MaxDamage = 7 + 2 * ItemLevel;
			Durability = 12 + 3 * ItemLevel;
			break;
		case WeaponType.Hammer:
			MinDamage = 4 + ItemLevel;
			MaxDamage = 12 + 3 * ItemLevel;
			Durability = 25 + 5 * ItemLevel;
			break;
		case WeaponType.LongSword:
			MinDamage = 7 + 2 * ItemLevel;
			MaxDamage = 10 + 2 * ItemLevel;
			Durability = 25 + 5 * ItemLevel;
			break;
		case WeaponType.Spear:
			MinDamage = 4 + 2 * ItemLevel;
			MaxDamage = 8 + 2 * ItemLevel;
			Durability = 25 + 5 * ItemLevel;
			break;
		}
		MinDamage = (int)(MinDamage * (1 - 0.1 * (t - 1)));
		MaxDamage = (int)(MaxDamage * (1 - 0.1 * (t2 - 1)));

		if (t3 <= 3)
		{
			DamageT = (DamageType)(1 << t3);
			MinDamage = (int)(MinDamage * 1.5);
			MaxDamage = (int)(MaxDamage * 1.5);
		}
		MaxDurability = Durability;
	}

    public int AttackDamage
    {
        get
        {
            return (int)Random.Range(MinDamage, MaxDamage);
        }
    }

    public new string GetInfo()
    {
        string returnValue = "Lv " + ItemLevel.ToString() + " ";
        switch (WeaponT)
        {
            case WeaponType.Ax:
                returnValue += "도끼\n";
                break;
            case WeaponType.Dagger:
                returnValue += "단검\n";
                break;
            case WeaponType.Hammer:
                returnValue += "망치\n";
                break;
            case WeaponType.LongSword:
                returnValue += "장검\n";
                break;
            case WeaponType.Spear:
                returnValue += "장창\n";
                break;
        }

        returnValue += "공 +" + MinDamage.ToString() + " ~ " + MaxDamage.ToString() + "\n";
        returnValue += "내구도 " + Durability + "/" + MaxDurability;
        return returnValue;
    }
}

public class ArmorClass : ItemClass
{
    public ArmorType ArmorT;
    public int Defence;
    public int AdditionalHP;
	public DamageType ImmuneT;

	public ArmorClass(ArmorType w, int Level)
	{
		ItemLevel = Level;
        ItemInfo = (int)w + 5;
		ArmorT = w;
        IType = ItemType.Armor;
		int t = (int)Random.Range (0, 3);
		int t2 = (int)Random.Range (0, 3);
		int t3 = (int)Random.Range (0, 100);
		int t4 = (int)Random.Range (0, 4);
		switch (w)
		{
		case ArmorType.Plate:
			Defence = 2 * ItemLevel;
			AdditionalHP = 10 * ItemLevel;
			Durability = 25 + 5 * ItemLevel;
			if (t3 <= 10)
				ImmuneT = (DamageType)(1 << t4);
			break;
		case ArmorType.Chain:
			Defence = 2 * ItemLevel;
			AdditionalHP = 0 * ItemLevel;
			Durability = 25 + 3 * ItemLevel;
			if (t3 <= 30)
				ImmuneT = (DamageType)(1 << t4);
			break;
		case ArmorType.Leather:
			Defence = ItemLevel;
			AdditionalHP = 15 * ItemLevel;
			Durability = 20 + 5 * ItemLevel;
			if (t3 <= 50)
				ImmuneT = (DamageType)(1 << t4);
			break;
		case ArmorType.SoftArmor:
			Defence = ItemLevel;
			AdditionalHP = 5 * ItemLevel;
			Durability = 20 + 5 * ItemLevel;
			if (t3 <= 70)
				ImmuneT = (DamageType)(1 << t4);
			break;
		}

		Defence = (int)(Defence * (1 - 0.1 * (t - 1)));
		AdditionalHP = (int)(AdditionalHP * (1 - 0.1 * (t2 - 1)));

		if (ImmuneT == DamageType.N)
		{
			Defence = (int)(Defence * 1.5);
		}
		MaxDurability = Durability;
	}

	public void ReLevel(int i)
	{
		ItemLevel += i;
		int t = (int)Random.Range (0, 3);
		int t2 = (int)Random.Range (0, 3);
		int t3 = (int)Random.Range (0, 100);
		int t4 = (int)Random.Range (0, 4);
		switch (ArmorT)
		{
		case ArmorType.Plate:
			Defence = 2 * ItemLevel;
			AdditionalHP = 10 * ItemLevel;
			Durability = 25 + 5 * ItemLevel;
			if (t3 <= 10)
				ImmuneT = (DamageType)(1 << t4);
			break;
		case ArmorType.Chain:
			Defence = 2 * ItemLevel;
			AdditionalHP = 0 * ItemLevel;
			Durability = 25 + 3 * ItemLevel;
			if (t3 <= 30)
				ImmuneT = (DamageType)(1 << t4);
			break;
		case ArmorType.Leather:
			Defence = ItemLevel;
			AdditionalHP = 15 * ItemLevel;
			Durability = 20 + 5 * ItemLevel;
			if (t3 <= 50)
				ImmuneT = (DamageType)(1 << t4);
			break;
		case ArmorType.SoftArmor:
			Defence = ItemLevel;
			AdditionalHP = 5 * ItemLevel;
			Durability = 20 + 5 * ItemLevel;
			if (t3 <= 70)
				ImmuneT = (DamageType)(1 << t4);
			break;
		}

		Defence = (int)(Defence * (1 - 0.1 * (t - 1)));
		AdditionalHP = (int)(AdditionalHP * (1 - 0.1 * (t2 - 1)));

		if (ImmuneT == DamageType.N)
		{
			Defence = (int)(Defence * 1.5);
		}
		MaxDurability = Durability;
	}

    public new string GetInfo()
    {
        string returnValue = "Lv " + ItemLevel.ToString() + " ";
        switch (ArmorT)
        {
            case ArmorType.Plate:
                returnValue += "판금\n";
                break;
            case ArmorType.Chain:
                returnValue += "사슬\n";
                break;
            case ArmorType.Leather:
                returnValue += "가죽\n";
                break;
            case ArmorType.SoftArmor:
                returnValue += "천\n";
                break;
        }

        returnValue += "방어력 +" + Defence.ToString() + "\n";
        returnValue += "내구도 " + Durability + "/" + MaxDurability;
        return returnValue;
    }
}

public delegate void del();

public class ConsumableClass : ItemClass
{
    public ConsumableType ConsumableT;
	public int UsableTiming;
	public del ItemEffect;
	public DamageType ItemDamageType;

	public ConsumableClass(ConsumableType T)
	{
		IType = ItemType.Consumable;
		ConsumableT = T;
		switch (T) {
		case ConsumableType.Bomb:
			ItemLevel = 1;
			UsableTiming = 0;
			ItemEffect = () => {
				
				int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
				int damage = (int)Random.Range (10, 21);
				MonsterClass.MonsterInstanceList [index].GetDamage (damage, DamageType.N);
			};
			break;
		case ConsumableType.Bandage:
			ItemLevel = 1;
			UsableTiming = 2;
			ItemEffect = () => {
				PlayerClass.PCDataInstance.hp += (int)(PlayerClass.PCDataInstance.maxhp * 0.15);
				if (PlayerClass.PCDataInstance.hp > PlayerClass.PCDataInstance.maxhp)
					PlayerClass.PCDataInstance.hp = PlayerClass.PCDataInstance.maxhp;
			};
			break;
		case ConsumableType.RepairTool1:
			ItemLevel = 2;
			UsableTiming = 1;
			ItemEffect = () => {
				PlayerClass.PCDataInstance.Armor.Durability	+= (int)(PlayerClass.PCDataInstance.Armor.MaxDurability * 0.25 * (ItemLevel - 1));
				PlayerClass.PCDataInstance.Weapon.Durability += (int)(PlayerClass.PCDataInstance.Armor.MaxDurability * 0.25 * (ItemLevel - 1));
				if (PlayerClass.PCDataInstance.Armor.Durability > PlayerClass.PCDataInstance.Armor.MaxDurability)
					PlayerClass.PCDataInstance.Armor.Durability = PlayerClass.PCDataInstance.Armor.MaxDurability;
				if (PlayerClass.PCDataInstance.Weapon.Durability > PlayerClass.PCDataInstance.Weapon.MaxDurability)
					PlayerClass.PCDataInstance.Weapon.Durability = PlayerClass.PCDataInstance.Weapon.MaxDurability;
			};
			break;
		case ConsumableType.RepairTool2:
			ItemLevel = 3;
			UsableTiming = 1;
			ItemEffect = () => {
				PlayerClass.PCDataInstance.Armor.Durability	+= (int)(PlayerClass.PCDataInstance.Armor.MaxDurability * 0.25 * (ItemLevel - 1));
				PlayerClass.PCDataInstance.Weapon.Durability += (int)(PlayerClass.PCDataInstance.Armor.MaxDurability * 0.25 * (ItemLevel - 1));
				if (PlayerClass.PCDataInstance.Armor.Durability > PlayerClass.PCDataInstance.Armor.MaxDurability)
					PlayerClass.PCDataInstance.Armor.Durability = PlayerClass.PCDataInstance.Armor.MaxDurability;
				if (PlayerClass.PCDataInstance.Weapon.Durability > PlayerClass.PCDataInstance.Weapon.MaxDurability)
					PlayerClass.PCDataInstance.Weapon.Durability = PlayerClass.PCDataInstance.Weapon.MaxDurability;
			};
			break;
		case ConsumableType.WeaponScroll:
			ItemLevel = 3;
			UsableTiming = 2;
			ItemEffect = () => {
				PlayerClass.PCDataInstance.Weapon.ReLevel (1);
			};
			break;
		case ConsumableType.ArmorScroll:
			ItemLevel = 3;
			UsableTiming = 2;
			ItemEffect = () => {
				PlayerClass.PCDataInstance.Armor.ReLevel (1);
			};
			break;
		case ConsumableType.BuffPotion1:
			ItemLevel = 3;
			UsableTiming = 0;
			ItemDamageType = DamageType.T1;
			ItemEffect = () => {
				PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType | ItemDamageType);
			};
			break;
		case ConsumableType.BuffPotion2:
			ItemLevel = 3;
			UsableTiming = 0;
			ItemDamageType = DamageType.T2;
			ItemEffect = () => {
				PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType | ItemDamageType);
			};
			break;
		case ConsumableType.BuffPotion3:
			ItemLevel = 3;
			UsableTiming = 0;
			ItemDamageType = DamageType.T3;
			ItemEffect = () => {
				PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType | ItemDamageType);
			};
			break;
		case ConsumableType.BuffPotion4:
			ItemLevel = 3;
			UsableTiming = 0;
			ItemDamageType = DamageType.T4;
			ItemEffect = () => {
				PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType | ItemDamageType);
			};
			break;
		case ConsumableType.DeBuffPotion1:
			ItemLevel = 3;
			UsableTiming = 0;
			ItemDamageType = DamageType.T1;
			ItemEffect = () => {
				int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
				MonsterClass.MonsterInstanceList [index].ImmuneType = (DamageType)(MonsterClass.MonsterInstanceList [index].ImmuneType & ~ItemDamageType);
			};
			break;
		case ConsumableType.DeBuffPotion2:
			ItemLevel = 3;
			UsableTiming = 0;
			ItemDamageType = DamageType.T2;
			ItemEffect = () => {
				int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
				MonsterClass.MonsterInstanceList [index].ImmuneType = (DamageType)(MonsterClass.MonsterInstanceList [index].ImmuneType & ~ItemDamageType);
			};
			break;
		case ConsumableType.DeBuffPotion3:
			ItemLevel = 3;
			UsableTiming = 0;
			ItemDamageType = DamageType.T3;
			ItemEffect = () => {
				int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
				MonsterClass.MonsterInstanceList [index].ImmuneType = (DamageType)(MonsterClass.MonsterInstanceList [index].ImmuneType & ~ItemDamageType);
			};
			break;
		case ConsumableType.DeBuffPotion4:
			ItemLevel = 3;
			UsableTiming = 0;
			ItemDamageType = DamageType.T4;
			ItemEffect = () => {
				int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
				MonsterClass.MonsterInstanceList [index].ImmuneType = (DamageType)(MonsterClass.MonsterInstanceList [index].ImmuneType & ~ItemDamageType);
			};
			break;
		case ConsumableType.HealPotion1:
			ItemLevel = 2;
			UsableTiming = 1;
			ItemEffect = () => {
				PlayerClass.PCDataInstance.hp += (int)(PlayerClass.PCDataInstance.maxhp * 0.25 * (ItemLevel - 1));
				if (PlayerClass.PCDataInstance.hp > PlayerClass.PCDataInstance.maxhp)
					PlayerClass.PCDataInstance.hp = PlayerClass.PCDataInstance.maxhp;
			};
			break;
		case ConsumableType.HealPotion2:
			ItemLevel = 3;
			UsableTiming = 1;
			ItemEffect = () => {
				PlayerClass.PCDataInstance.hp += (int)(PlayerClass.PCDataInstance.maxhp * 0.25 * (ItemLevel - 1));
				if (PlayerClass.PCDataInstance.hp > PlayerClass.PCDataInstance.maxhp)
					PlayerClass.PCDataInstance.hp = PlayerClass.PCDataInstance.maxhp;
			};
			break;
		case ConsumableType.Plague:
			ItemLevel = 4;
			UsableTiming = 0;
			ItemEffect = () => {
				
				int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
				MonsterClass.MonsterInstanceList [index].defence = (int)(MonsterClass.MonsterInstanceList [index].defence * 0.5);
			};
			break;
		case ConsumableType.Treasure:
			ItemLevel = 5;
			UsableTiming = 2;
			break;
		}
	}

    public new string GetInfo()
    {
        string returnValue = "Lv " + ItemLevel.ToString() + " ";
        switch (ConsumableT)
        {
            case ConsumableType.Bomb:
                returnValue += "폭탄\n";
                break;
            case ConsumableType.Bandage:
                returnValue += "붕대\n";
                break;
            case ConsumableType.RepairTool1:
                returnValue += "저급 수리 도구\n";
                break;
            case ConsumableType.RepairTool2:
                returnValue += "고급 수리 도구\n";
                break;
            case ConsumableType.WeaponScroll:
                returnValue += "무기 강화 스크롤\n";
                break;
            case ConsumableType.ArmorScroll:
                returnValue += "방어구 강화 스크롤\n";
                break;
            case ConsumableType.BuffPotion1:
                returnValue += "화염의 면역 포션\n";
                break;
            case ConsumableType.BuffPotion2:
                returnValue += "빙하의 면역 포션\n";
                break;
            case ConsumableType.BuffPotion3:
                returnValue += "바람의 면역 포션\n";
                break;
            case ConsumableType.BuffPotion4:
                returnValue += "대지의 면역 포션\n";
                break;
            case ConsumableType.DeBuffPotion1:
                returnValue += "화염의 취약 포션\n";
                break;
            case ConsumableType.DeBuffPotion2:
                returnValue += "빙하의 취약 포션\n";
                break;
            case ConsumableType.DeBuffPotion3:
                returnValue += "바람의 취약 포션\n";
                break;
            case ConsumableType.DeBuffPotion4:
                returnValue += "대지의 취약 포션\n";
                break;
            case ConsumableType.HealPotion1:
                returnValue += "소형 회복 포션\n";
                break;
            case ConsumableType.HealPotion2:
                returnValue += "대형 회복 포션\n";
                break;
            case ConsumableType.Plague:
                returnValue += "부식 포션\n";
                break;
            case ConsumableType.Treasure:
                returnValue += "보물\n";
                break;
        }
        return returnValue;
    }

    public ConsumableClass(int i)
	{
		int a;
		ItemLevel = i;
        IType = ItemType.Consumable;
		switch (i) {
		case 1:
			a = Random.Range(0, 2);
			if (a == 0)
			{
				ConsumableT = ConsumableType.Bomb;
				UsableTiming = 0;
				ItemEffect = () => {
					
					int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
					int damage = (int)Random.Range (10, 21);
					MonsterClass.MonsterInstanceList [index].GetDamage (damage, DamageType.N);
				};
			}
			else
			{
				ConsumableT = ConsumableType.Bandage;
				UsableTiming = 2;
				ItemEffect = () => {
					PlayerClass.PCDataInstance.hp += (int)(PlayerClass.PCDataInstance.maxhp * 0.25);
					if (PlayerClass.PCDataInstance.hp > PlayerClass.PCDataInstance.maxhp)
						PlayerClass.PCDataInstance.hp = PlayerClass.PCDataInstance.maxhp;
				};
			}
			break;
		case 2:
			a = Random.Range(0, 2);
			if (a == 0)
			{
				ConsumableT = ConsumableType.HealPotion1;
				UsableTiming = 1;
				ItemEffect = () => {
					PlayerClass.PCDataInstance.hp += (int)(PlayerClass.PCDataInstance.maxhp * 0.25 * (ItemLevel - 1));
					if (PlayerClass.PCDataInstance.hp > PlayerClass.PCDataInstance.maxhp)
						PlayerClass.PCDataInstance.hp = PlayerClass.PCDataInstance.maxhp;
				};
			}
			else
			{
				ConsumableT = ConsumableType.RepairTool1;
				UsableTiming = 1;
				ItemEffect = () => {
					PlayerClass.PCDataInstance.Armor.Durability	+= (int)(PlayerClass.PCDataInstance.Armor.MaxDurability * 0.25 * (ItemLevel - 1));
					PlayerClass.PCDataInstance.Weapon.Durability += (int)(PlayerClass.PCDataInstance.Weapon.MaxDurability * 0.25 * (ItemLevel - 1));
					if (PlayerClass.PCDataInstance.Armor.Durability > PlayerClass.PCDataInstance.Armor.MaxDurability)
						PlayerClass.PCDataInstance.Armor.Durability = PlayerClass.PCDataInstance.Armor.MaxDurability;
					if (PlayerClass.PCDataInstance.Weapon.Durability > PlayerClass.PCDataInstance.Weapon.MaxDurability)
						PlayerClass.PCDataInstance.Weapon.Durability = PlayerClass.PCDataInstance.Weapon.MaxDurability;
				};
			}
			break;
		case 3:
			a = Random.Range(0, 12);
			switch (a) {
			case 0:
				ConsumableT = ConsumableType.HealPotion2;
				UsableTiming = 1;
				ItemEffect = () => {
					PlayerClass.PCDataInstance.hp += (int)(PlayerClass.PCDataInstance.maxhp * 0.25 * (ItemLevel - 1));
					if (PlayerClass.PCDataInstance.hp > PlayerClass.PCDataInstance.maxhp)
						PlayerClass.PCDataInstance.hp = PlayerClass.PCDataInstance.maxhp;
				};
				break;
			case 1:
				ConsumableT = ConsumableType.RepairTool2;
				UsableTiming = 1;
				ItemEffect = () => {
					PlayerClass.PCDataInstance.Armor.Durability	+= (int)(PlayerClass.PCDataInstance.Armor.MaxDurability * 0.25 * (ItemLevel - 1));
					PlayerClass.PCDataInstance.Weapon.Durability += (int)(PlayerClass.PCDataInstance.Weapon.MaxDurability * 0.25 * (ItemLevel - 1));
					if (PlayerClass.PCDataInstance.Armor.Durability > PlayerClass.PCDataInstance.Armor.MaxDurability)
						PlayerClass.PCDataInstance.Armor.Durability = PlayerClass.PCDataInstance.Armor.MaxDurability;
					if (PlayerClass.PCDataInstance.Weapon.Durability > PlayerClass.PCDataInstance.Weapon.MaxDurability)
						PlayerClass.PCDataInstance.Weapon.Durability = PlayerClass.PCDataInstance.Weapon.MaxDurability;
				};
				break;
			case 2:
				ConsumableT = ConsumableType.WeaponScroll;
				UsableTiming = 2;
				ItemEffect = () => {
					PlayerClass.PCDataInstance.Weapon.ReLevel (1);
				};
				break;
			case 3:
				ConsumableT = ConsumableType.ArmorScroll;
				UsableTiming = 2;
				ItemEffect = () => {
					PlayerClass.PCDataInstance.Armor.ReLevel (1);
				};
				break;
			case 4:
				ConsumableT = ConsumableType.BuffPotion1;
				ItemLevel = 3;
				UsableTiming = 0;
				ItemDamageType = DamageType.T1;
				ItemEffect = () => {
					PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType | ItemDamageType);
				};
				break;
			case 5:
				ConsumableT = ConsumableType.BuffPotion2;
				ItemLevel = 3;
				UsableTiming = 0;
				ItemDamageType = DamageType.T2;
				ItemEffect = () => {
					PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType | ItemDamageType);
				};
				break;
			case 6:
				ConsumableT = ConsumableType.BuffPotion3;
				ItemLevel = 3;
				UsableTiming = 0;
				ItemDamageType = DamageType.T3;
				ItemEffect = () => {
					PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType | ItemDamageType);
				};
				break;
			case 7:
				ConsumableT = ConsumableType.BuffPotion4;
				ItemLevel = 3;
				UsableTiming = 0;
				ItemDamageType = DamageType.T4;
				ItemEffect = () => {
					PlayerClass.PCDataInstance.ImmuneType = (DamageType)(PlayerClass.PCDataInstance.ImmuneType | ItemDamageType);
				};
				break;
			case 8:
				ConsumableT = ConsumableType.DeBuffPotion1;
				ItemLevel = 3;
				UsableTiming = 0;
				ItemDamageType = DamageType.T1;
				ItemEffect = () => {
					int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
					MonsterClass.MonsterInstanceList [index].ImmuneType = (DamageType)(MonsterClass.MonsterInstanceList [index].ImmuneType & ~ItemDamageType);
				};
				break;
			case 9:
				ConsumableT = ConsumableType.DeBuffPotion2;
				ItemLevel = 3;
				UsableTiming = 0;
				ItemDamageType = DamageType.T2;
				ItemEffect = () => {
					int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
					MonsterClass.MonsterInstanceList [index].ImmuneType = (DamageType)(MonsterClass.MonsterInstanceList [index].ImmuneType & ~ItemDamageType);
				};
				break;
			case 10:
				ConsumableT = ConsumableType.DeBuffPotion3;
				ItemLevel = 3;
				UsableTiming = 0;
				ItemDamageType = DamageType.T3;
				ItemEffect = () => {
					int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
					MonsterClass.MonsterInstanceList [index].ImmuneType = (DamageType)(MonsterClass.MonsterInstanceList [index].ImmuneType & ~ItemDamageType);
				};
				break;
			case 11:
				ConsumableT = ConsumableType.DeBuffPotion4;
				ItemLevel = 3;
				UsableTiming = 0;
				ItemDamageType = DamageType.T4;
				ItemEffect = () => {
					int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
					MonsterClass.MonsterInstanceList [index].ImmuneType = (DamageType)(MonsterClass.MonsterInstanceList [index].ImmuneType & ~ItemDamageType);
				};
				break;
			default:
				break;
			}
			break;
		case 4:
			ItemLevel = 4;
			UsableTiming = 0;
			ItemEffect = () => {
				int index = (int)Random.Range (0, MonsterClass.MonsterInstanceList.Count);
				MonsterClass.MonsterInstanceList [index].defence = (int)(MonsterClass.MonsterInstanceList [index].defence * 0.5);
			};
			break;
		case 5:
			ItemLevel = 5;
			UsableTiming = 2;
			break;
		default:
			break;
		}
        ItemInfo = (int)ConsumableT + 9;
	}
}

public enum ItemType
{
    Weapon, Armor, Consumable
}

public enum WeaponType
{
    Dagger = 0, LongSword = 1, Ax = 2, Spear = 3, Hammer = 4
}
public enum ArmorType
{
    Plate = 0, SoftArmor = 1, Leather = 2, Chain = 3
}
public enum ConsumableType
{
	Bomb = 0,
    Bandage = 1,
    HealPotion1 = 2, HealPotion2 = 3,
    RepairTool1 = 4, RepairTool2 = 5,
    WeaponScroll = 6, ArmorScroll,
	BuffPotion1, BuffPotion2, BuffPotion3, BuffPotion4, 
	DeBuffPotion1, DeBuffPotion2, DeBuffPotion3, DeBuffPotion4,
	Plague,
	Treasure
}
