﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClass : MonoBehaviour {

	public static PCData PCDataInstance;

	public class PCData
	{
		public int hp;
		public int maxhp;
		public List<ItemClass> Inventory = new List<ItemClass>(6);
		public List<BlessClass> BlessList = new List<BlessClass> (5);
		public DamageType ImmuneType;
		public WeaponClass Weapon;
		public ArmorClass Armor;
        public DamageType PowerType { get { return Weapon.DamageT; } }

        public bool OneKillImmune;

		// 기타 스텟

		public PCData()
		{
			BlessList = BlessClass.InitialBlessList();
			maxhp = 100;
			hp = maxhp;
			OneKillImmune = false;
			PCDataInstance = this;
			
			Weapon = new WeaponClass((WeaponType)(int)Random.Range(0, 5), 1);
			Armor = new ArmorClass((ArmorType)(int)Random.Range(0, 4), 1);
            GameObject.Find("GameController").GetComponent<DisplayManager>().UpdatePlayerEquip();
		}

		public int Power
		{
			get
			{
				if (Weapon != null)
					return 1 + Weapon.AttackDamage ;
				else
					return 1;
			}
		}

		public DamageType IsImmune(DamageType t)
		{
			return ImmuneType | t;
		}

		public void Pray()
		{
			
            int rndIdx = (int)Random.Range(0, 5);
			BlessList[rndIdx].Effect();
			GameObject.Find("PrayEffectText").GetComponentInChildren<TextMesh>().text = BlessList[rndIdx].GetInfo();

        }

		public void GetDamage(int p, DamageType pt)
		{
			GetDamage (p, pt, 1);
		}

		public void GetDamage(int p, DamageType pt, int option)
		{
			int def = 0;
			if (Armor != null)
				def = Armor.Defence;
			
			if (pt == DamageType.N)
			{
				hp = hp - (int)Mathf.Max (0f, (float)(p - def * option));
			}
			else
			{
				if ((pt & (DamageType.A ^ ImmuneType)) != 0)
				{
					hp = hp - (int)Mathf.Max (0, (float)(p - def * option));
				}
			}
		}

		public int UsingItem(int index)
		{
			if (index >= Inventory.Count)
				return 4;

			if (Inventory [index].IType == ItemType.Armor)
			{
				ItemClass temp = Armor;
				Armor = (ArmorClass)Inventory [index];
				Inventory [index] = temp;
				return 0;
			} 
			else if (Inventory [index].IType == ItemType.Weapon) 
			{
				ItemClass temp = Weapon;
   				Weapon = (WeaponClass)Inventory [index];    
				Inventory [index] = temp;
				return 0;
			}
			else 
			{
				return 1;
			}
		}

		public void ArmorTear()
		{
            if (Armor != null)
            {
                Armor.Durability -= Random.Range(0, 2);
                if(Armor.IsBroken())
                {
                    Armor = null;
                }
            }
		}

		public void WeaponTear()
		{
            if (Weapon != null)
            {
                Weapon.Durability -= Random.Range(0, 3);
                if (Weapon.IsBroken())
                {
                    Weapon = null;
                }
            }
		}

		public bool IsDead()
		{
			if (hp <= 0)
				return true;
			return false;
		}
	}

}
