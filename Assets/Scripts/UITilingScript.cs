﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITilingScript : MonoBehaviour {

    public string spritePrefixName;
    private GameObject[] spriteList;
    // Use this for initialization
    void Start()
    {
        spriteList = new GameObject[9];
        spriteList[0] = transform.Find(spritePrefixName + "_top_left").gameObject;
        spriteList[1] = transform.Find(spritePrefixName + "_top").gameObject;
        spriteList[2] = transform.Find(spritePrefixName + "_top_right").gameObject;
        spriteList[3] = transform.Find(spritePrefixName + "_left").gameObject;
        spriteList[4] = transform.Find(spritePrefixName + "_center").gameObject;
        spriteList[5] = transform.Find(spritePrefixName + "_right").gameObject;
        spriteList[6] = transform.Find(spritePrefixName + "_bottom_left").gameObject;
        spriteList[7] = transform.Find(spritePrefixName + "_bottom").gameObject;
        spriteList[8] = transform.Find(spritePrefixName + "_bottom_right").gameObject;

        SpriteRenderer mainSprite = GetComponent<SpriteRenderer>();
        SpriteRenderer subSprite;

        subSprite = spriteList[0].GetComponent<SpriteRenderer>();
        float subWidth = subSprite.sprite.rect.width * subSprite.transform.localScale.x / 100;
        float subHeight = subSprite.sprite.rect.height * subSprite.transform.localScale.y / 100;
        float mainWidth = mainSprite.bounds.size.x - subWidth;
        float mainHeight = mainSprite.bounds.size.y - subHeight;
        spriteList[0].transform.localPosition = new Vector3(-mainWidth / 2, mainHeight / 2);

        subSprite = spriteList[1].GetComponent<SpriteRenderer>();
        spriteList[1].transform.localPosition = new Vector3(0, mainHeight / 2);
        spriteList[1].transform.localScale = new Vector3((mainWidth - subWidth) / subSprite.sprite.rect.width * 100, spriteList[2].transform.localScale.y);

        subSprite = spriteList[2].GetComponent<SpriteRenderer>();
        spriteList[2].transform.localPosition = new Vector3(mainWidth / 2, mainHeight / 2);

        subSprite = spriteList[3].GetComponent<SpriteRenderer>();
        spriteList[3].transform.localPosition = new Vector3(-mainWidth / 2, 0);
        spriteList[3].transform.localScale = new Vector3(spriteList[3].transform.localScale.x, (mainHeight - subHeight) / subSprite.sprite.rect.height * 100);

        subSprite = spriteList[4].GetComponent<SpriteRenderer>();
        spriteList[4].transform.localPosition = new Vector3(0, 0);
        spriteList[4].transform.localScale = new Vector3((mainWidth - subWidth) / subSprite.sprite.rect.width * 100, (mainHeight - subHeight) / subSprite.sprite.rect.height * 100);

        subSprite = spriteList[5].GetComponent<SpriteRenderer>();
        spriteList[5].transform.localPosition = new Vector3(mainWidth / 2, 0);
        spriteList[5].transform.localScale = new Vector3(spriteList[5].transform.localScale.x, (mainHeight - subHeight) / subSprite.sprite.rect.height * 100);

        subSprite = spriteList[6].GetComponent<SpriteRenderer>();
        spriteList[6].transform.localPosition = new Vector3(-mainWidth / 2, -mainHeight / 2);

        subSprite = spriteList[7].GetComponent<SpriteRenderer>();
        spriteList[7].transform.localPosition = new Vector3(0, -mainHeight / 2);
        spriteList[7].transform.localScale = new Vector3((mainWidth - subWidth) / subSprite.sprite.rect.width * 100, spriteList[7].transform.localScale.y);

        subSprite = spriteList[8].GetComponent<SpriteRenderer>();
        spriteList[8].transform.localPosition = new Vector3(mainWidth / 2, -mainHeight / 2);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
