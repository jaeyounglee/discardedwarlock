﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterClass
{
	public static List<MonsterClass> MonsterInstanceList;
    public int hp;
    public int maxhp;
    public int defence;
	public int Level;
	public int MinPower;
	public int MaxPower;
	public int ListIndex;
	public bool OneKillImmune;
    public MonsterType MonsterT;

	public SkillType SkillT;
    public DamageType PowerType;
    public DamageType ImmuneType;

    public static MonsterType RandomMonsterType(int lvl)
    {
        lvl -= (Random.Range(0.0f,1.0f) < 0.5f) ? 0 : 1;
        lvl = Mathf.Min(lvl, 4);
        lvl = Mathf.Max(lvl, 0);
        return (MonsterType) lvl;
    }

    public MonsterClass(int lvl) : this(lvl, RandomMonsterType(lvl)) { }

	public MonsterClass(int lvl, MonsterType T)
	{
		OneKillImmune = false;
		int p;
        Level = lvl;
        MonsterT = T;
		switch (T)
		{
		case MonsterType.Slime:
			hp = 15;
			p = PowerDecider (Level);
			if (p == 1)
				PowerType = (DamageType)(1 << (int)Random.Range(0, 4));
			if (ImmuneDecider (Level) == 1)
				ImmuneType = (DamageType)(1 << (int)Random.Range(0, 4));
			MinPower = (int)(3 * (1 + p * 0.5));
			MaxPower = (int)(5 * (1 + p * 0.5));
			defence = 0;
			SkillT = SkillType.SelfHeal;
			break;
		case MonsterType.Goblin:
			hp = 10 + 6 * Level;
			p = PowerDecider (Level);
			if (p == 1)
				PowerType = (DamageType)(1 << (int)Random.Range(0, 4));
			if (ImmuneDecider (Level) == 1)
				ImmuneType = (DamageType)(1 << (int)Random.Range(0, 4));
			MinPower = (int)((4 + Level) * (1 + p * 0.5));
			MaxPower = (int)((5 + 2 * Level) * (1 + p * 0.5));
			defence = 1;
			break;
		case MonsterType.Skeleton:
			hp = 8 + 6 * Level;
			p = PowerDecider (Level);
			if (p == 1)
				PowerType = (DamageType)(1 << (int)Random.Range(0, 4));
			if (ImmuneDecider (Level) == 1)
				ImmuneType = (DamageType)(1 << (int)Random.Range(0, 4));
			MinPower = (int)((4 + 2 * Level) * (1 + p * 0.5));
			MaxPower = (int)((6 + 2 * Level) * (1 + p * 0.5));
			defence = 1 + Level;
			SkillT = SkillType.UndeadHeal;
			break;
		case MonsterType.Spirit:
			hp = 12 + 9 * Level;
			p = PowerDecider (Level);
			if (p == 1)
				PowerType = (DamageType)(1 << (int)Random.Range(0, 4));
			if (ImmuneDecider (Level) == 1)
				ImmuneType = (DamageType)(1 << (int)Random.Range(0, 4));
			MinPower = (int)((2 + Level) * (1 + p * 0.5));
			MaxPower = (int)((4 + Level) * (1 + p * 0.5));
			defence = 3;
			SkillT = SkillType.Penetrate;
			break;
		case MonsterType.Demon:
			hp = 4 + 8 * Level;
			p = PowerDecider (Level);
			if (p == 1)
				PowerType = (DamageType)(1 << (int)Random.Range(0, 4));
			if (ImmuneDecider (Level) == 1)
				ImmuneType = (DamageType)(1 << (int)Random.Range(0, 4));
			MinPower = (int)((1 + 2 * Level) * (1 + p * 0.5));
			MaxPower = (int)((1 + 3 * Level) * (1 + p * 0.5));
			defence = Level;
			SkillT = SkillType.RemoveSpirit;
			break;
		case MonsterType.Golem:
			Level = 10;
			hp = 50;
			MinPower = 10;
			MaxPower = 30;
			defence = 0;
			SkillT = SkillType.BlessOfSpirit;
			OneKillImmune = true;
			ImmuneType = DamageType.T1 | DamageType.T2 | DamageType.T3 | DamageType.T4;
			break;
		case MonsterType.Dragon:
			Level = 10;
			hp = 150;
			MinPower = 25;
			MaxPower = 35;
			defence = 10;
			OneKillImmune = true;
			ImmuneType = (DamageType)(1 << (int)Random.Range (0, 4));
			DamageType t;
			while (true)
			{
				t = (DamageType)(1 << (int)Random.Range (0, 4));
				if (t != ImmuneType)
				{
					ImmuneType = ImmuneType | t;
					break;
				}
			}	
			SkillT = SkillType.Revenge;
			break;
		default:
			break;
		}
		ListIndex = MonsterInstanceList.Count;
        maxhp = hp;
		MonsterInstanceList.Add(this);
	}

    public int Power
    {
        get
        {
			
			return (int)Random.Range(MinPower, MaxPower);
        }
    }

	public void GetDamage(int p, DamageType pt)
	{
		GetDamage (p, pt, 1);
	}

	public void GetDamage(int p, DamageType pt, int option)
    {
		if (SkillT == SkillType.BlessOfSpirit)
			defence += 5 * BitCounter ();

		if (pt == DamageType.N)
		{
			hp = hp - (int)Mathf.Max (0f, (float)(p - defence * option));
		}
		else
		{
			if ((pt & (DamageType.A ^ ImmuneType)) != 0)
			{
				hp = hp - (int)Mathf.Max (0, (float)(p - defence * option));
			}
		}

		if (SkillT == SkillType.BlessOfSpirit)
			defence -= 5 * BitCounter ();
    }
    
	public SkillType Skill() 
	{
		return SkillT;
    }

    public bool IsBossType
    {
        get
        {
            if (MonsterT == MonsterType.Golem)
                return true;
            if (MonsterT == MonsterType.Dragon)
                return true;
            return false;
        }
    }

	public bool IsDead
    {
        get
        {
            return hp <= 0;
        }
	}

	private int PowerDecider(int L)
	{
		

		if (L * 20 - 10 >= (int)Random.Range (0, 100))
			return 1;
		else
			return 0;
	}
	private int ImmuneDecider(int L)
	{
		

		if (L * 20 >= (int)Random.Range (0, 100))
			return 1;
		else
			return 0;
	}

	private int BitCounter()
	{
		int c = 0;
		int im = (int)ImmuneType;
		for (int i = 0; i < 4; i++)
		{
			if ((im & 1) == 1)
				c++;
			im <<= 1;
		}
		return c;
	}
}

public enum MonsterType
{
    Slime, Goblin, Skeleton, Spirit, Demon, Golem, Dragon
}

public enum SkillType
{
	SelfHeal, UndeadHeal, Penetrate, RemoveSpirit, BlessOfSpirit, Revenge
}